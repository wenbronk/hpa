package cn.ktt.hpa.test.server.oa;

import cn.ktt.hpa.server.ihrm.IhrmApplication;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @Author wenbronk <meng.wen@kangtaitong.cn>
 * @Date 2019-10-14 16:40
 * description:
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = IhrmApplication.class)
public class BaseSpringbootTest {

}
