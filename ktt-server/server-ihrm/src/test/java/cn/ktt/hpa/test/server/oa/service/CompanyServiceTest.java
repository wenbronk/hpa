package cn.ktt.hpa.test.server.oa.service;

import cn.ktt.hpa.server.model.ihrm.po.Company;
import cn.ktt.hpa.server.ihrm.service.impl.CompanyService;
import cn.ktt.hpa.server.model.ihrm.enums.CompanyAudState;
import cn.ktt.hpa.server.model.ihrm.enums.CompanyAvaliableState;
import cn.ktt.hpa.server.model.ihrm.po.Department;
import cn.ktt.hpa.test.server.oa.BaseSpringbootTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * @Author wenbronk <meng.wen@kangtaitong.cn>
 * @Date 2019-10-14 17:09
 * description:
 */
public class CompanyServiceTest extends BaseSpringbootTest {

    @Autowired
    private CompanyService companyService;

    @Test
    public void testSave() throws Exception {
        for (int i = 0; i < 1; i++) {
            Company company = new Company();
            company.setCompanyName("ktt" + i);
            company.setManagerId("234234213423");
            company.setExpirationDate(new Date());
            company.setCompanyProvince("江苏");
            company.setCompanyCity("宿迁");
            company.setCompanyPhone("18888888888");
            company.setCompanySize("big");
            company.setIndustry("养老医疗");
            company.setAuditState(CompanyAudState.AUDITEDFAILED);
            company.setAvaliableState(CompanyAvaliableState.ENABLED);
            companyService.save(company);
        }
    }

    @Test
//    @Modifying
//    @Transactional
//    @Rollback(false)
    public void testUpdate() throws Exception {
        Company company = new Company();
        company.setId(633409736162672640L);
        company.setCompanyName("kttWenbronk");
        company.setTenantId(12424214214214234L);
        company.setManagerId("12424214214214234L");
        company.setDeleteTime(new Date());
        companyService.update(company);
    }

    @Test
    public void testFindById() {
        Company companyBy = companyService.findCompanyById(633355305060663296L);
//        System.out.println(companyBy);

        List<Department> departmentList = companyBy.getDepartmentList();
        System.out.println(departmentList);

    }

    @Test
    public void testDelete() {
        companyService.deleteById(633409736162672640L);
    }

}
