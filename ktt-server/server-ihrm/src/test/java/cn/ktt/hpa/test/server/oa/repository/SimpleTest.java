package cn.ktt.hpa.test.server.oa.repository;

import org.junit.Test;

import java.util.Arrays;
import java.util.stream.Stream;

/**
 * @Author wenbronk <meng.wen@kangtaitong.cn>
 * @Date 2019-10-09 16:46
 * description:
 */
public class SimpleTest {
    public static void main(String[] args) {
        String abc = "631532468356251648";
        System.out.println(abc.length());
    }

    @Test
    public void teset2() {
        String[] ignoreProperties = {"a", "b", "c", "d"};
        final String[] IGNORE_PROPERTIES = {"deleteTime", "tenantId"};
        if (ignoreProperties == null || ignoreProperties.length == 0) {
            ignoreProperties = IGNORE_PROPERTIES;
        } else {
            ignoreProperties = Stream.concat(Arrays.stream(ignoreProperties), Arrays.stream(IGNORE_PROPERTIES)).toArray(String[]::new);
        }

        Arrays.stream(ignoreProperties).forEach(System.out::println);
    }
}
