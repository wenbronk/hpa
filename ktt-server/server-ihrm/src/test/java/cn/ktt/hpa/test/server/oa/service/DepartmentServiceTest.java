package cn.ktt.hpa.test.server.oa.service;

import cn.ktt.hpa.server.ihrm.repository.DepartmentRepository;
import cn.ktt.hpa.server.model.ihrm.po.Company;
import cn.ktt.hpa.server.model.ihrm.po.Department;
import cn.ktt.hpa.test.server.oa.BaseSpringbootTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @Author wenbronk <meng.wen@kangtaitong.cn>
 * @Date 2019-10-17 09:02
 * description:
 */
public class DepartmentServiceTest extends BaseSpringbootTest {

    @Autowired
    private DepartmentRepository departmentRepository;

    @Test
    public void save() {
        for (int i = 0; i < 10; i++) {
            Department department = new Department();
            department.setPid(0L);
            department.setName("dep" + i);
            department.setCode("aaa" + i);
            department.setManager("wenbronk");
            department.setManagerId(12342314L);
            department.setIntroduce("hello world");
            Company company = new Company();
            company.setId(633355305060663296L);
            department.setCompany(company);
            departmentRepository.save(department);
        }
    }

    @Test
    public void testDelete() {
        Department department = new Department();
        department.setId(635858069569404928L);
        departmentRepository.delete(department);
    }

    @Test
    public void testFind() {
        List<Department> departments = departmentRepository.findAll();
        departments.forEach(department -> {
            Company company = department.getCompany();
            System.out.println(company.getCompanyName());
        });
    }

}
