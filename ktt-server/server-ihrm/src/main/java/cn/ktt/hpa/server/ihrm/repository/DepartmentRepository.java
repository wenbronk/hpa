package cn.ktt.hpa.server.ihrm.repository;

import cn.ktt.hpa.server.model.ihrm.po.Department;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @Author wenbronk <meng.wen@kangtaitong.cn>
 * @Date 2019-10-15 16:48
 * description:
 */
public interface DepartmentRepository extends JpaRepository<Department, Long> {
}
