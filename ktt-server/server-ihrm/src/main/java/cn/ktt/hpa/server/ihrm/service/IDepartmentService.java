package cn.ktt.hpa.server.ihrm.service;

import cn.ktt.hpa.server.model.ihrm.bo.DepartmentBO;
import cn.ktt.hpa.server.model.ihrm.po.Department;

import java.util.List;
import java.util.Optional;

/**
 * @Author wenbronk <meng.wen@kangtaitong.cn>
 * @Date 2019-10-17 09:03
 * description:
 */
public interface IDepartmentService {

    Department save(Department department) throws Exception;

    Department update(Department department) throws Exception;

    Department findCompanyById(Long departmentId);

    List<Department> findAll();

    void deleteById(Long departmentId);

}
