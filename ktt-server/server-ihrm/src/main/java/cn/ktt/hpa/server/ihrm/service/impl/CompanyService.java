package cn.ktt.hpa.server.ihrm.service.impl;

import cn.ktt.hpa.server.model.ihrm.bo.CompanyBO;
import cn.ktt.hpa.server.model.ihrm.po.Company;
import cn.ktt.hpa.server.ihrm.repository.CompanyRepository;
import cn.ktt.hpa.server.ihrm.service.ICompanyService;
import cn.ktt.hpa.server.model.ihrm.enums.CompanyAudState;
import cn.ktt.hpa.server.model.ihrm.enums.CompanyAvaliableState;
import lombok.extern.log4j.Log4j2;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

/**
 * @Author wenbronk <meng.wen@kangtaitong.cn>
 * @Date 2019-10-14 16:44
 * description:
 */
@Service
@Log4j2
public class CompanyService implements ICompanyService {

    @Autowired
    private CompanyRepository companyRepository;

    @Override
    public Company save(Company company) throws Exception {

        company.setTenantId(UUID.randomUUID().toString());
        return companyRepository.save(company);
    }

    @Override
    public Company update(Company company) throws Exception {
//        Company company = company.transfor(Company::new);
        return companyRepository.save(company);
    }

    @Override
    public Company findCompanyById(Long companyId) {
        return companyRepository.findById(companyId).orElseGet(null);
    }

    @Override
    public List<Company> findAll() {
        return companyRepository.findAll();
    }

    @Override
    public void deleteById(Long companyId) {
        companyRepository.deleteById(companyId);
    }


}
