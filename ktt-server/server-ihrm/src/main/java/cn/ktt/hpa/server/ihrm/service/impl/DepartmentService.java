package cn.ktt.hpa.server.ihrm.service.impl;

import cn.ktt.hpa.server.ihrm.repository.DepartmentRepository;
import cn.ktt.hpa.server.ihrm.service.IDepartmentService;
import cn.ktt.hpa.server.model.ihrm.bo.DepartmentBO;
import cn.ktt.hpa.server.model.ihrm.po.Department;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @Author wenbronk <meng.wen@kangtaitong.cn>
 * @Date 2019-10-17 09:03
 * description:
 */
public class DepartmentService implements IDepartmentService {

    @Autowired
    private DepartmentRepository departmentRepository;

    @Override
    public Department save(Department department) throws Exception{
        return departmentRepository.save(department);
    }

    @Override
    public Department update(Department department) throws Exception {
        return departmentRepository.save(department.transfor(Department::new));
    }

    @Override
    public List<Department> findAll() {
        return departmentRepository.findAll();
    }

    @Override
    public Department findCompanyById(Long departmentId) {
        return departmentRepository.findById(departmentId).orElseGet(null);
    }


    @Override
    public void deleteById(Long departmentId) {
        departmentRepository.deleteById(departmentId);
    }

}
