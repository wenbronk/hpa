package cn.ktt.hpa.server.ihrm;

import cn.ktt.hpa.commons.db.jpa.softdelete.EnableJpaSoftDeleteRepositories;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

/**
 * @Author wenbronk <meng.wen@kangtaitong.cn>
 * @Date 2019-10-09 09:35
 * description:
 */
@SpringBootApplication
@EntityScan(basePackages = {"cn.ktt.hpa.server.model.ihrm", "cn.ktt.hpa.commons.base.model"})
@ComponentScan(basePackages = {"cn.ktt.hpa.server.ihrm", "cn.ktt.hpa.commons"})
@EnableJpaSoftDeleteRepositories(value = {"cn.ktt.hpa.server.ihrm.repository"})
//@EnableMongoRepositories(basePackages = {"com.wenbronk.framework.repository.cms"})
public class IhrmApplication {

    public static void main(String[] args) {
        SpringApplication.run(IhrmApplication.class, args);
    }

}
