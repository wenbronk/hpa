package cn.ktt.hpa.server.ihrm.service;

import cn.ktt.hpa.commons.entity.TransforException;
import cn.ktt.hpa.server.model.ihrm.bo.CompanyBO;
import cn.ktt.hpa.server.model.ihrm.po.Company;

import java.util.List;

/**
 * @Author wenbronk <meng.wen@kangtaitong.cn>
 * @Date 2019-10-14 16:41
 * description:
 */
public interface ICompanyService {

    Company save(Company company) throws TransforException, Exception;

    Company update(Company company) throws Exception;

    Company findCompanyById(Long companyId);

    List<Company> findAll();

    void deleteById(Long companyId);

    // TODO
    // 设置审核状态变更， 可用状态变更， 设置余额等

}
