package cn.ktt.hpa.server.ihrm.repository;

import cn.ktt.hpa.server.model.ihrm.po.Company;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @Author wenbronk <meng.wen@kangtaitong.cn>
 * @Date 2019-10-14 16:37
 * description:
 */
public interface CompanyRepository extends JpaRepository<Company, Long> {
}
