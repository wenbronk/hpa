package cn.ktt.hpa.api.orgm;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @Author wenbronk <meng.wen@kangtaitong.cn>
 * @Date 2019-10-09 11:15
 * description:
 */
@Api(value = "用户信息管理中心", tags = "用户相关操作")
public interface IUserController {

    @ApiOperation(value = "保存用户信息", notes = "保存用户信息")
    Boolean save();

}
