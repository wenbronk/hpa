package cn.ktt.hpa.api.orgm;

import cn.ktt.hpa.commons.entity.TransforException;
import cn.ktt.hpa.commons.response.BaseResponse;
import cn.ktt.hpa.server.model.orgm.form.KeyForm;
import cn.ktt.hpa.server.model.orgm.form.company.CompanyArgsForm;
import cn.ktt.hpa.server.model.orgm.form.company.CompanyEditForm;
import cn.ktt.hpa.server.model.orgm.form.company.CompanyNewForm;
import cn.ktt.hpa.server.model.orgm.po.Company;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "组织相关操作", tags = "组织相关操作")
public interface ICompanyController {

    @ApiOperation(value = "获取所有组织", notes = "获取所有组织")
    BaseResponse findAll() throws TransforException;

    @ApiOperation(value = "获取指定组织", notes = "获取指定组织")
    BaseResponse getOne(KeyForm form) throws TransforException;

    @ApiOperation(value = "新增组织", notes = "新增组织")
    BaseResponse add(CompanyNewForm form);

    @ApiOperation(value = "更新组织", notes = "更新组织")
    BaseResponse update(CompanyEditForm form);

    @ApiOperation(value = "删除组织", notes = "删除组织")
    BaseResponse delete(KeyForm form);

    @ApiOperation(value = "变更余额", notes = "变更余额")
    BaseResponse changeBalance(CompanyArgsForm form);

    @ApiOperation(value = "审核", notes = "审核")
    BaseResponse audit(CompanyArgsForm form);

    @ApiOperation(value = "可用", notes = "可用")
    BaseResponse enable(CompanyArgsForm form);
}
