
dependencies {
    implementation(project(":sys-commons:wb-commons-entity"))
    implementation(project(":ktt-server:business-utils"))
    implementation(project(":ktt-server:business-model"))

    implementation(project(":sys-commons:wb-swagger2-springboot-starter"))
    implementation(project(":sys-commons:wb-base-spring-boot-starter"))
//    implementation("io.springfox", "springfox-swagger-ui")
//    implementation("io.springfox", "springfox-swagger2")


}
