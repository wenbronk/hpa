package cn.ktt.hpa.server.orgm.service.impl;

import cn.ktt.hpa.commons.entity.CollectionTransfor;
import cn.ktt.hpa.commons.entity.TransforException;
import cn.ktt.hpa.server.model.orgm.enums.CompanyAudState;
import cn.ktt.hpa.server.model.orgm.enums.CompanyAvaliableState;
import cn.ktt.hpa.server.model.orgm.po.Company;
import cn.ktt.hpa.server.model.orgm.vo.CompanyVO;
import cn.ktt.hpa.server.orgm.repository.CompanyRepository;
import cn.ktt.hpa.server.orgm.service.ICompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class CompanyService implements ICompanyService {

    @Autowired
    private CompanyRepository companyRepository;

    @Override
    public boolean save(Company company) {
        company.setAuditState(CompanyAudState.UNAUDITED);
        company.setAvaliableState(CompanyAvaliableState.PREPARING);
        company.setBalance(0d);
        companyRepository.save(company);

        return true;
    }

    @Override
    public boolean update(Company company) {
        companyRepository.save(company);
        return true;
    }

    @Override
    public CompanyVO findCompanyById(Long companyId) throws TransforException {
        Company company = companyRepository.findById(companyId).orElseGet(null);
        return company != null ? company.toVO(CompanyVO.class) : null;
    }

    @Override
    public List<CompanyVO> findAll() throws TransforException {
        return CollectionTransfor.convert(companyRepository.findAll(),CompanyVO.class);
    }

    @Override
    public boolean deleteById(Long companyId) {
        companyRepository.deleteById(companyId);
        return true;
    }

    @Transactional
    @Override
    public boolean changeBalance(Long companyId, Double money) {
        Optional<Company> optCompany = companyRepository.findById(companyId);
        if (optCompany.isPresent()) {
            Company company = optCompany.get();
            company.setBalance(money);
            companyRepository.save(company);
            return true;
        }
        return false;
    }

    @Transactional
    @Override
    public boolean audit(Long companyId, CompanyAudState audState) {

        Optional<Company> optCompany = companyRepository.findById(companyId);
        if (optCompany.isPresent()) {
            Company company = optCompany.get();
            company.setAuditState(audState);
            companyRepository.save(company);
            return true;
        }
        return false;
    }

    @Transactional
    @Override
    public boolean enable(Long companyId, CompanyAvaliableState avaliableState) {

        Optional<Company> optCompany = companyRepository.findById(companyId);
        if (optCompany.isPresent()) {
            Company company = optCompany.get();
            company.setAvaliableState(avaliableState);
            companyRepository.save(company);
            return true;
        }
        return false;
    }
}
