package cn.ktt.hpa.server.orgm.service;

import cn.ktt.hpa.commons.entity.TransforException;
import cn.ktt.hpa.server.model.orgm.enums.CompanyAudState;
import cn.ktt.hpa.server.model.orgm.enums.CompanyAvaliableState;
import cn.ktt.hpa.server.model.orgm.po.Company;
import cn.ktt.hpa.server.model.orgm.vo.CompanyVO;

import java.util.List;

public interface ICompanyService {

    boolean save(Company company);

    boolean update(Company company);

    CompanyVO findCompanyById(Long companyId) throws TransforException;

    List<CompanyVO> findAll() throws TransforException;

    boolean deleteById(Long companyId);

    boolean changeBalance(Long companyId,Double money);

    boolean audit(Long companyId,CompanyAudState audState);

    boolean enable(Long companyId, CompanyAvaliableState avaliableState);
}
