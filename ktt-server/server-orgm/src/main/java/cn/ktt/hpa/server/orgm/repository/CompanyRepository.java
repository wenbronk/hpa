package cn.ktt.hpa.server.orgm.repository;

import cn.ktt.hpa.server.model.orgm.po.Company;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompanyRepository extends JpaRepository<Company,Long> {

}
