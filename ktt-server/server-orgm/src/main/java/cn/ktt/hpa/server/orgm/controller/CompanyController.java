package cn.ktt.hpa.server.orgm.controller;

import cn.ktt.hpa.api.orgm.ICompanyController;
import cn.ktt.hpa.commons.entity.TransforException;
import cn.ktt.hpa.commons.response.BaseResponse;
import cn.ktt.hpa.commons.response.CommonsCode;
import cn.ktt.hpa.server.model.orgm.form.KeyForm;
import cn.ktt.hpa.server.model.orgm.form.company.CompanyArgsForm;
import cn.ktt.hpa.server.model.orgm.form.company.CompanyEditForm;
import cn.ktt.hpa.server.model.orgm.form.company.CompanyNewForm;
import cn.ktt.hpa.server.model.orgm.po.Company;
import cn.ktt.hpa.server.model.orgm.vo.CompanyVO;
import cn.ktt.hpa.server.orgm.service.ICompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/company",method = {RequestMethod.POST},produces = "application/json;charset=utf-8", consumes = "application/json;charset=utf-8")
public class CompanyController implements ICompanyController {

    @Autowired
    private ICompanyService companyService;

    @RequestMapping("/findAll")
    @Override
    public BaseResponse findAll() throws TransforException {

        List<CompanyVO> vos = companyService.findAll();
        return BaseResponse.getResponse(CommonsCode.SUCCESS, vos);
    }

    @PostMapping("/getOne")
    @Override
    public BaseResponse getOne(@RequestBody @Valid KeyForm form) throws TransforException {

        CompanyVO vo = companyService.findCompanyById(form.getId());
        return BaseResponse.getResponse(CommonsCode.SUCCESS, vo);
    }

    @RequestMapping("/add")
    @Override
    public BaseResponse add(@RequestBody @Validated CompanyNewForm form) {

        companyService.save(form.toPO(Company.class));
        return BaseResponse.getResponse(CommonsCode.SUCCESS,true);
    }

    @RequestMapping("/update")
    @Override
    public BaseResponse update(@RequestBody @Validated CompanyEditForm form){

        companyService.update(form.toPO(Company.class));
        return BaseResponse.getResponse(CommonsCode.SUCCESS,true);
    }

    @RequestMapping("/delete")
    @Override
    public BaseResponse delete(@RequestBody @Validated KeyForm form){
        companyService.deleteById(form.getId());
        return BaseResponse.getResponse(CommonsCode.SUCCESS,true);
   }

    @RequestMapping("/changeBalance")
    @Override
    public BaseResponse changeBalance(@RequestBody @Validated CompanyArgsForm form){
        companyService.changeBalance(form.getId(),form.getMoney());
        return BaseResponse.getResponse(CommonsCode.SUCCESS,true);
    }

    @RequestMapping("/audit")
    @Override
    public BaseResponse audit(@RequestBody @Validated CompanyArgsForm form){
        companyService.audit(form.getId(),form.getAudState());
        return BaseResponse.getResponse(CommonsCode.SUCCESS,true);
    }

    @RequestMapping("/enable")
    @Override
    public BaseResponse enable(@RequestBody @Validated CompanyArgsForm form){
        companyService.enable(form.getId(),form.getAvaliableState());
        return BaseResponse.getResponse(CommonsCode.SUCCESS,true);
    }
}
