package cn.ktt.hpa.server.orgm.enums;

import cn.ktt.hpa.commons.response.ResultCode;

/**
 * @Author lei.yan
 * @Date 2019/11/5 17:30
 * @Version 1.0
 * description:
 */
public enum  CompanyResultCode implements ResultCode {
    /**
     *
     */
    COMPANY_NAME_IS_EMPTY(true, 210001, "公司名称为空！");

    private boolean success;
    private int code;
    private String message;

    private CompanyResultCode(boolean success, int code, String message) {
        this.success = success;
        this.code = code;
        this.message = message;
    }

    @Override
    public boolean success() {
        return this.success;
    }

    @Override
    public int code() {
        return this.code;
    }

    @Override
    public String message() {
        return this.message;
    }
}
