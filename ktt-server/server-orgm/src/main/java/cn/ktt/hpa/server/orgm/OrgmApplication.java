package cn.ktt.hpa.server.orgm;


import cn.ktt.hpa.commons.db.jpa.softdelete.EnableJpaSoftDeleteRepositories;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

import static org.springframework.boot.SpringApplication.run;


@SpringBootApplication
@EntityScan(basePackages = {"cn.ktt.hpa.server.model.orgm", "cn.ktt.hpa.commons.base.model"})
@ComponentScan(basePackages = {"cn.ktt.hpa.api.orgm", "cn.ktt.hpa.server.orgm", "cn.ktt.hpa.commons"})
@EnableJpaSoftDeleteRepositories(value = {"cn.ktt.hpa.server.orgm.repository"})
public class OrgmApplication {
    public static void main(String[] args) {
        run(OrgmApplication.class, args);
    }
}
