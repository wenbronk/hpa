import org.springframework.boot.gradle.tasks.run.BootRun

plugins {
    id("org.springframework.boot")
}
springBoot {
    mainClassName = "cn.ktt.hpa.server.orgm.OrgmApplication"
}
//val profile = System.getProperty("env") ?: "prod"
//tasks.getByName<BootRun>("bootRun") {
//    systemProperty("spring.profiles.active", "${profile}")
//    main = "cn.ktt.hpa.server.ihrm.IhrmApplication"
//}

springBoot {
    buildInfo {
        properties {
            artifact = "${project.name}"
            version = "${project.version}"
            group = "${project.group}"
            name = "office auto application"
        }
    }
}

dependencies {

    implementation(project(":ktt-server:business-utils"))
    implementation(project(":ktt-server:business-model"))
    implementation(project(":ktt-server:business-api"))
    implementation(project(":sys-commons:wb-base-spring-boot-starter"))
    implementation(project(":sys-commons:wb-db-spring-boot-starter"))
    implementation(project(":sys-commons:wb-commons-entity"))

    implementation("org.springframework.boot:spring-boot-starter")
    implementation("org.springframework.boot:spring-boot-starter-web")
    testCompile("org.springframework.boot:spring-boot-starter-test")

    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation("org.springframework.boot:spring-boot-starter-jdbc")
    implementation("mysql:mysql-connector-java")
}