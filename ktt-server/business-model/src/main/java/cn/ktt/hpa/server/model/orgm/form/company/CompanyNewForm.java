package cn.ktt.hpa.server.model.orgm.form.company;

import cn.ktt.hpa.commons.base.enums.PsersistEnumValueDeserializer;
import cn.ktt.hpa.commons.entity.form.AbstractEntityForm;
import cn.ktt.hpa.server.model.orgm.enums.CompanyAudState;
import cn.ktt.hpa.server.model.orgm.enums.CompanyAvaliableState;
import com.alibaba.fastjson.annotation.JSONField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @Author lei.yan
 * @Date 2019/11/6 14:13
 * @Version 1.0
 * description:
 */
@ApiModel
@Data
@EqualsAndHashCode(callSuper = true)
public class CompanyNewForm extends AbstractEntityForm {

    public CompanyNewForm(){
        this.auditState = CompanyAudState.UNAUDITED;
        this.avaliableState = CompanyAvaliableState.PREPARING;
        this.balance = 0d;
    }

    @ApiModelProperty(value = "机构名称",example = "xxx养老院")
    @NotNull
    private String companyName;

    @NotNull
    @ApiModelProperty(value = "机构企业登陆账号",example = "")
    private String managerId;

    @NotNull
    @ApiModelProperty(value = "过期日期",example = "")
    private Date expirationDate;

    @NotNull
    @ApiModelProperty(value = "所属省",example = "")
    private String companyProvince;

    @NotNull
    @ApiModelProperty(value = "所属市",example = "")
    private String companyCity;

    @NotNull
    @ApiModelProperty(value = "详细地址",example = "")
    private String companyDetailAddress;

    @NotNull
    @ApiModelProperty(value = "营业执照",example = "")
    private String businessLicenseId;

    @NotNull
    @ApiModelProperty(value = "法人代表",example = "")
    private String legalRepresentative;

    @NotNull
    @ApiModelProperty(value = "电话",example = "")
    private String companyPhone;

    @NotNull
    @ApiModelProperty(value = "邮箱",example = "")
    private String mailbox;

    @ApiModelProperty(value = "机构规模",example = "")
    private String companySize;

    @ApiModelProperty(value = "行业",example = "")
    private String industry;

    @ApiModelProperty(value = "备注",example = "")
    private String description;

    @ApiModelProperty(value = "审核状态",example = "")
    @JSONField(deserializeUsing = PsersistEnumValueDeserializer.class)
    private CompanyAudState auditState;

    @ApiModelProperty(value = "机构可用状态",example = "")
    @JSONField(deserializeUsing = PsersistEnumValueDeserializer.class)
    private CompanyAvaliableState avaliableState;

    @ApiModelProperty(value = "账号余额",example = "")
    private Double balance;
}
