package cn.ktt.hpa.server.model.orgm.vo;

import cn.ktt.hpa.commons.base.enums.PsersistEnumValueSerializer;
import cn.ktt.hpa.commons.entity.vo.AbstractVO;
import cn.ktt.hpa.server.model.orgm.enums.CompanyAudState;
import cn.ktt.hpa.server.model.orgm.enums.CompanyAvaliableState;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Data
public class CompanyVO extends AbstractVO {

    private String companyName;

    private String managerId;

    @JSONField(format = "yyyy-MM-dd")
    private Date expirationDate;

    private String companyProvince;

    private String companyCity;

    private String companyDetailAddress;

    private String businessLicenseId;

    private String legalRepresentative;

    private String companyPhone;

    private String mailbox;

    private String companySize;

    private String industry;

    private String description;

    @JSONField(serializeUsing = PsersistEnumValueSerializer.class)
    private CompanyAudState auditState;

    @JSONField(serializeUsing = PsersistEnumValueSerializer.class)
    private CompanyAvaliableState avaliableState;

    private Double balance;

    private String tenantId;
}
