package cn.ktt.hpa.server.model.orgm.form;

import cn.ktt.hpa.commons.entity.form.AbstractForm;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.NotNull;

/**
 * @Author lei.yan
 * @Date 2019/11/6 9:43
 * @Version 1.0
 * description:
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class KeyForm extends AbstractForm {

    @NotNull
    private Long id;
}
