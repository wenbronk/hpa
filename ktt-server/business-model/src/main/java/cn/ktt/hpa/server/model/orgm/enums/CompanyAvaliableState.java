package cn.ktt.hpa.server.model.orgm.enums;

import cn.ktt.hpa.commons.base.enums.AbstractEnumConverter;
import cn.ktt.hpa.commons.base.enums.PersistEnum;
import cn.ktt.hpa.commons.base.enums.PsersistEnumValueDeserializer;
import cn.ktt.hpa.commons.base.enums.PsersistEnumValueSerializer;
import com.alibaba.fastjson.annotation.JSONType;

/**
 * @Author wenbronk <meng.wen@kangtaitong.cn>
 * @Date 2019-10-15 17:27
 * description:
 */
public enum CompanyAvaliableState implements PersistEnum<Integer> {

    /**
     *
     */
    PREPARING("preparing", 1, "审核中"),
    ENABLED("enabled", 3, "可用"),
    STOP("stop", 6, "暂停使用"),
    SUSPEND("suspend", 9, "中止使用");

    private String name;
    private Integer code;
    private String description;

    private CompanyAvaliableState(String name, Integer code, String description) {
        this.name = name;
        this.code = code;
        this.description = description;
    }

    @Override
    public Integer getCode() {
        return code;
    }

    public static class Convert extends AbstractEnumConverter<CompanyAvaliableState, Integer> {}

}