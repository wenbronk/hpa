package cn.ktt.hpa.server.model.orgm.form.company;

import cn.ktt.hpa.commons.entity.form.AbstractKeyForm;
import cn.ktt.hpa.server.model.orgm.enums.CompanyAudState;
import cn.ktt.hpa.server.model.orgm.enums.CompanyAvaliableState;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Author lei.yan
 * @Date 2019/11/6 9:57
 * @Version 1.0
 * description:
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class CompanyArgsForm extends AbstractKeyForm {

    private Double money = 0d;

    private CompanyAudState audState =  CompanyAudState.UNAUDITED;

    private CompanyAvaliableState avaliableState = CompanyAvaliableState.PREPARING;
}
