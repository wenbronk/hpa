package cn.ktt.hpa.server.model.ihrm.po;

import cn.ktt.hpa.commons.entity.po.BaseEntity;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.Where;
import org.springframework.context.annotation.Lazy;

import javax.persistence.*;

/**
 * @Author wenbronk <meng.wen@kangtaitong.cn>
 * @Date 2019-10-14 13:40
 * description: 部门信息
 */
@Data
@Entity
@Table(name = "sys_department")
@EqualsAndHashCode(callSuper = true)
@ToString(exclude = "company")
public class Department extends BaseEntity {

    @Column(name = "pid", columnDefinition = "bigint(20) NOT NULL COMMENT '父级ID'")
    private Long pid;

    @Column(name = "name", columnDefinition = "varchar(50) NOT NULL COMMENT '部门名称'")
    private String name;

    @Column(name = "code", columnDefinition = "varchar(50) NOT NULL COMMENT '部门编码'")
    private String code;

    @Column(name = "manager_id", columnDefinition = "bigint(20) NOT NULL COMMENT '负责人ID'")
    private Long managerId;

    @Column(name = "manager", columnDefinition = "varchar(50) NOT NULL COMMENT '负责人名称'")
    private String manager;

    @Column(name = "introduce", columnDefinition = "varchar(50) COMMENT '介绍'")
    private String introduce;

    @Lazy
    @Where(clause = NOT_DELETED)
    @ManyToOne(targetEntity = Company.class, cascade = CascadeType.DETACH)
    @JoinColumn(name = "company_id")
    private Company company;

}
