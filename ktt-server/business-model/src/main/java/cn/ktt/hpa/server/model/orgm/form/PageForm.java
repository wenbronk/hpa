package cn.ktt.hpa.server.model.orgm.form;

import cn.ktt.hpa.commons.entity.form.AbstractPageForm;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class PageForm extends AbstractPageForm {

}
