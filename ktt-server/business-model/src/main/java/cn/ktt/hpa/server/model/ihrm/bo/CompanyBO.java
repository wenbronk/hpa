package cn.ktt.hpa.server.model.ihrm.bo;

import cn.ktt.hpa.commons.entity.bo.AbstractBO;
import cn.ktt.hpa.server.model.ihrm.enums.CompanyAudState;
import cn.ktt.hpa.server.model.ihrm.enums.CompanyAvaliableState;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;
import java.util.List;

/**
 * @Author wenbronk <meng.wen@kangtaitong.cn>
 * @Date 2019-10-10 16:26
 * description: 公司信息
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class CompanyBO extends AbstractBO {

    private String companyName;

    private String managerId;

    private Date expirationDate;

    private String companyProvince;

    private String companyCity;

    private String companyDetailAddress;

    private String businessLicenseId;

    private String legalRepresentative;

    private String companyPhone;

    private String mailbox;

    private String companySize;

    private String industry;

    private String description;

    private CompanyAudState auditState;

    private CompanyAvaliableState avaliableState;

    private Double balance;

    private List<DepartmentBO> departmentList;

}
