package cn.ktt.hpa.server.model.ihrm.bo;

import cn.ktt.hpa.commons.entity.po.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

/**
 * @Author wenbronk <meng.wen@kangtaitong.cn>
 * @Date 2019-10-14 13:40
 * description: 部门信息
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class DepartmentBO extends BaseEntity {

    private Long pid;

    private String name;

    private String code;

    private Long managerId;

    private String manager;

    private String introduce;

    private CompanyBO company;

}
