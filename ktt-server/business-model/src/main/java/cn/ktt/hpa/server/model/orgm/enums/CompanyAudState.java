package cn.ktt.hpa.server.model.orgm.enums;

import cn.ktt.hpa.commons.base.enums.AbstractEnumConverter;
import cn.ktt.hpa.commons.base.enums.PersistEnum;
import cn.ktt.hpa.commons.base.enums.PsersistEnumValueDeserializer;
import cn.ktt.hpa.commons.base.enums.PsersistEnumValueSerializer;
import com.alibaba.fastjson.annotation.JSONType;

/**
 * @Author wenbronk <meng.wen@kangtaitong.cn>
 * @Date 2019-10-15 17:25
 * description:
 */

public enum CompanyAudState implements PersistEnum<Integer> {

    /**
     *
     */
    UNAUDITED("unadudited", 2, "未审核"),
    AUDITEDSUCCESS("auditedSuccess", 5, "审核通过"),
    AUDITEDFAILED("auditedFailed", 8, "审核未通过");

    private String name;
    private Integer code;
    private String description;

    private CompanyAudState(String name, Integer code, String description) {
        this.name = name;
        this.code = code;
        this.description = description;
    }

    @Override
    public Integer getCode() {
        return this.code;
    }

    public static class Converter extends AbstractEnumConverter<CompanyAudState, Integer> {}
}
