package cn.ktt.hpa.server.model.dummy;

import cn.ktt.hpa.commons.entity.po.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @Author wenbronk <meng.wen@kangtaitong.cn>
 * @Date 2019-10-10 16:24
 * description:
 */

@Data
@Entity
@Table(name = "employee")
@EqualsAndHashCode(callSuper = true)
//@Table(uniqueConstraints = { @UniqueConstraint(columnNames = { "username" }),
//        @UniqueConstraint(columnNames = { "email" }) })
//@ToString(exclude = {"roles"})
//@SQLDelete(sql = "update #{#entityName} set deleteTag = true where id = ?")
//@Where(clause = "delete_tag = false")
//@SQLDelete(sql = "update {table_name} set deleteTag = 1 where id = ?")
public class Example extends BaseEntity {

    private String nickName;

}
