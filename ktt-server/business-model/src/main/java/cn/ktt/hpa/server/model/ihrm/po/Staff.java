package cn.ktt.hpa.server.model.ihrm.po;

import cn.ktt.hpa.commons.base.model.SysUser;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

/**
 * @Author wenbronk <meng.wen@kangtaitong.cn>
 * @Date 2019-10-09 09:45
 * description:
 */
@Data
@Entity
@Table(name = "employee")
@EqualsAndHashCode(callSuper = true)
//@ToString(exclude = {"roles"})
public class Staff extends SysUser {

    /**反三范式设计*/
    @Column(name = "company_name", columnDefinition = "varchar(20) NOT NULL COMMENT '公司名称'")
    private String companyName;

    @Column(name = "department_name", columnDefinition = "varchar(20) NOT NULL COMMENT '部门名称'")
    private String departmentName;

    /**
     * 所属公司
     */
    @ManyToOne(targetEntity = Company.class, cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    @JoinColumn(name = "company_id")
    private Company company;

    /**
     * 所属部门
     */
    @ManyToOne(targetEntity = Department.class, cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    @JoinColumn(name = "department_id")
    private Department department;

    @Column(name = "dummy_tenant_id", columnDefinition = "bigint(20) COMMENT '可伪装的id'")
    private Long dummyTenantId;

}
