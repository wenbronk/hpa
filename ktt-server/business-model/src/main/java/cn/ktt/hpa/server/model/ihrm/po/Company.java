package cn.ktt.hpa.server.model.ihrm.po;

import cn.ktt.hpa.commons.entity.po.BaseEntity;
import cn.ktt.hpa.server.model.ihrm.enums.CompanyAudState;
import cn.ktt.hpa.server.model.ihrm.enums.CompanyAvaliableState;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;
import org.springframework.context.annotation.Lazy;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * @Author wenbronk <meng.wen@kangtaitong.cn>
 * @Date 2019-10-10 16:26
 * description: 公司信息
 */
@Data
@Entity
@Table(name = "sys_company")
@EqualsAndHashCode(callSuper = true)
public class Company extends BaseEntity {

    @Column(name = "company_name", columnDefinition = "varchar(50) NOT NULL COMMENT '公司名称'")
    private String companyName;

    @Column(name = "company_id", columnDefinition = "varchar(50) NOT NULL COMMENT '企业登录账号ID'")
    private String managerId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "expiration_date", columnDefinition = "timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '过期时间'")
    private Date expirationDate;

    @Column(name = "company_province", columnDefinition = "varchar(50) NOT NULL COMMENT '公司所在省'")
    private String companyProvince;

    @Column(name = "company_city", columnDefinition = "varchar(50) NOT NULL COMMENT '公司所在市'")
    private String companyCity;

    @Column(name = "company_detail_address", columnDefinition = "varchar(200) COMMENT '公司详细地址'")
    private String companyDetailAddress;

    @Column(name = "business_license_id", columnDefinition = "varchar(50) COMMENT '公司营业执照'")
    private String businessLicenseId;

    @Column(name = "legal_representative", columnDefinition = "varchar(20) COMMENT '法人代表'")
    private String legalRepresentative;

    @Column(name = "company_phone", columnDefinition = "varchar(20) NOT NULL COMMENT '公司电话'")
    private String companyPhone;

    @Column(name = "mailbox", columnDefinition = "varchar(30) COMMENT '邮箱'")
    private String mailbox;

    @Column(name = "company_size", columnDefinition = "varchar(20) NOT NULL COMMENT '公司规模'")
    private String companySize;

    @Column(name = "industry", columnDefinition = "varchar(20) NOT NULL COMMENT '所属行业'")
    private String industry;

    @Column(name = "description", columnDefinition = "varchar(20) COMMENT '备注'")
    private String description;

    @Column(name = "audit_state", columnDefinition = "int NOT NULL COMMENT '审核状态'")
    @Convert(converter = CompanyAudState.Converter.class)
    private CompanyAudState auditState;

    @Column(name = "avaliable_state", columnDefinition = "int NOT NULL COMMENT '状态'")
    @Convert(converter = CompanyAvaliableState.Convert.class)
    private CompanyAvaliableState avaliableState;

    @Column(name = "balance", columnDefinition = "double COMMENT '账户余额'")
    private Double balance;

    @Lazy
    @Where(clause = NOT_DELETED)
    @OneToMany(mappedBy = "company", cascade = CascadeType.DETACH)
    private List<Department> departmentList;

}
