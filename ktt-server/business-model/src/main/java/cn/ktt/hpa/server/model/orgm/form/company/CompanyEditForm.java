package cn.ktt.hpa.server.model.orgm.form.company;

import cn.ktt.hpa.commons.base.enums.PsersistEnumValueDeserializer;
import cn.ktt.hpa.server.model.orgm.enums.CompanyAudState;
import cn.ktt.hpa.server.model.orgm.enums.CompanyAvaliableState;
import cn.ktt.hpa.server.model.orgm.form.KeyForm;
import com.alibaba.fastjson.annotation.JSONField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * @Author lei.yan
 * @Date 2019/11/6 14:13
 * @Version 1.0
 * description:
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class CompanyEditForm extends KeyForm {

    @ApiModelProperty(value = "机构名称",example = "xxx养老院")
    private String companyName;

    @ApiModelProperty(value = "机构企业登陆账号",example = "")
    private String managerId;

    @ApiModelProperty(value = "过期日期",example = "")
    private Date expirationDate;

    @ApiModelProperty(value = "所属省",example = "")
    private String companyProvince;

    @ApiModelProperty(value = "所属市",example = "")
    private String companyCity;

    @ApiModelProperty(value = "详细地址",example = "")
   private String companyDetailAddress;

    @ApiModelProperty(value = "营业执照",example = "")
    private String businessLicenseId;

    @ApiModelProperty(value = "法人代表",example = "")
    private String legalRepresentative;

    @ApiModelProperty(value = "电话",example = "")
    private String companyPhone;

    @ApiModelProperty(value = "邮箱",example = "")
    private String mailbox;

    @ApiModelProperty(value = "机构规模",example = "")
    private String companySize;

    @ApiModelProperty(value = "行业",example = "")
    private String industry;

    @ApiModelProperty(value = "备注",example = "")
    private String description;

    @ApiModelProperty(value = "审核状态",example = "")
    @JSONField(deserializeUsing = PsersistEnumValueDeserializer.class)
    private CompanyAudState auditState;

    @ApiModelProperty(value = "机构可用状态",example = "")
    @JSONField(deserializeUsing = PsersistEnumValueDeserializer.class)
    private CompanyAvaliableState avaliableState;

    @ApiModelProperty(value = "账号余额",example = "")
    private Double balance;

    @ApiModelProperty(value = "机构编号",example = "")
    private String tenantId;
}
