
dependencies {
//    api(project(":ktt-server:business-utils"))
    implementation(project(":sys-commons:wb-commons-entity"))
    implementation(project(":ktt-server:business-utils"))
    implementation(project(":sys-commons:wb-base-spring-boot-starter"))
    implementation(project(":sys-commons:wb-db-spring-boot-starter"))

    implementation("org.springframework.boot:spring-boot-starter")
    implementation("org.springframework.boot:spring-boot-starter-validation")
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")

    implementation("io.springfox", "springfox-swagger-ui")
    implementation("io.springfox", "springfox-swagger2")

//    implementation("org.mybatis.spring.boot:mybatis-spring-boot-starter")
}
