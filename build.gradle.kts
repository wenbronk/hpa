import org.jetbrains.kotlin.gradle.plugin.KotlinSourceSet
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

val jdkVersion: String by project
val groupName: String by project
val projectVersion: String by project

val profile = System.getProperty("env") ?: "dev"
//apply(from = "build-${env}.gradle.kts")

//System.setProperty("spring.profiles.active", "${profile}")


//tasks.withType<BootRun>().configureEach {
//    systemProperty("spring.profiles.active", "${profile}")
//}

//applicationDefaultJvmArgs = [
//    "-Dspring.profiles.active=${project.gradle.startParameter.systemPropertiesArgs['spring.profiles.active']}"
//]

//tasks.named("build") {
//    doFirst{
//        System.setProperty("spring.profiles.active", "${profile}")
//    }
//    doLast {
//        print(System.getProperty("spring.profiles.active"))
//    }
//}

buildscript {
    val kotlinVersion by extra("1.3.50")
    val springBootVersion by extra("2.1.8.RELEASE")
    val springDependencyVersion by extra("1.0.8.RELEASE")

    repositories {
        mavenCentral()
        jcenter()
    }

    dependencies {
        classpath(kotlin("gradle-plugin", version = "${kotlinVersion}"))
        classpath("org.springframework.boot:spring-boot-gradle-plugin:${springBootVersion}")
//        classpath("io.spring.gradle:dependency-management-plugin:${springDependencyVersion}")
    }
}

plugins {
    // base
    // application
    // idea
    java
    maven
//    kotlin("jvm") version "1.3.50"
//    id("org.springframework.boot") version "2.1.8.RELEASE" apply false
    id("maven-publish")
    id("io.spring.dependency-management") version "1.0.8.RELEASE"
}

//application {
//    applicationDefaultJvmArgs = listOf("-Dspring.profiles.active=${profile}")
//}

allprojects {
    apply(plugin = "org.gradle.java")
    apply(plugin = "org.gradle.idea")

    apply(plugin = "org.gradle.maven")
    apply(plugin = "maven-publish")

    group = "$groupName"
    version = "$projectVersion"

}

subprojects {
    apply(plugin = "org.gradle.java")
    apply(plugin = "org.jetbrains.kotlin.jvm")
    apply(plugin = "io.spring.dependency-management")

    configure<JavaPluginConvention> {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    tasks.withType<KotlinCompile> {
        kotlinOptions.jvmTarget = "1.8"
    }

//    tasks.withType<KotlinCompile>().configureEach {
//        println("Configuring $name in project ${project.name}...")
//        kotlinOptions {
//            suppressWarnings = true
//        }
//    }

    repositories {
        mavenLocal()
        jcenter { mavenContent { releasesOnly() } }
        maven {
            url = uri("http://maven.aliyun.com/nexus/content/groups/public")
            content {
                excludeGroupByRegex("cn\\.ktt.*")
            }
        }
        maven {
            url = uri("http://nexus.kangtaitong.cn:8081/nexus/content/groups/public/")
        }
    }

    tasks.register<Jar>("sourcesJar") {
        from(sourceSets.main.get().allJava)
        archiveClassifier.set("sources")
    }

    tasks.register<Jar>("javadocJar") {
        from(tasks.javadoc)
        archiveClassifier.set("javadoc")
    }

    // 发布标示
    val nexusUrl: String by project
    val NEXUS_USERNAME: String by project
    val NEXUS_PASSWORD: String by project
    publishing {
        publications {
            create<MavenPublication>("maven") {
                groupId = "${project.group}"
                artifactId = "${project.name}"
                version = "${project.version}"
                from(components["java"])
                artifact(tasks["sourcesJar"])
                artifact(tasks["javadocJar"])
            }
        }

        repositories {
            maven {
                val name = "nexus"
                val releaseRepoUrl = uri("${nexusUrl}/repositories/releases/")
                val snapshotsRepoUrl = uri("${nexusUrl}/repositories/snapshots/")
                url = if (project.version.toString().endsWith("-SNAPSHOT")) snapshotsRepoUrl else releaseRepoUrl

                if (profile == "test") {
                    credentials {
                        username = "${NEXUS_USERNAME}"
                        password = "${NEXUS_PASSWORD}"
                    }
                }
            }
        }
    }

    sourceSets {
        main {
            java {
                setSrcDirs(listOf("src/main/java"))
                // exclude 'some/unwanted/package/**'
            }
            withConvention(KotlinSourceSet::class) {
                kotlin.srcDir("src/main/kotlin")
            }
            resources {
                srcDirs(listOf("src/main/profile/${profile}", "src/main/resources"))
            }
        }
        test {
            java {
                setSrcDirs(listOf("src/test/java"))
            }
        }
    }

//    (tasks.getByName("processResources") as ProcessResources).apply {
//        filter<ReplaceTokens>("profile" to profile)
//    }
//    val processResources by tasks.getting(ProcessResources::class) {
//        filesMatching("**/application.yml") {
//            filter { it.replace("profile", profile as String) }
////            filter { it.replace("%APP_DESCRIPTION%", project.description ?: "") }
//        }
//    }


    val lombokVersion by extra("1.18.6")
    val swagger2Version by extra("2.9.2")
    val guavaVersion by extra("28.1-jre")
    val mybatisVersion by extra("2.1.0")
    val mysqlVersion by extra("5.1.48")
    val aspectjVersion by extra("1.6.11")
    val fastjsonVersion by extra("1.2.62")

    dependencyManagement {
        //        // 禁用重写
//        // overriddenByDependencies(false)
        imports {
            mavenBom(org.springframework.boot.gradle.plugin.SpringBootPlugin.BOM_COORDINATES)
        }

//        禁用排除 exclusion
//        applyMavenExclusions(false)
//        configurations {
//            "implementation" {
//                exclude(group = "org.springframework.boot", module = "spring-boot-starter-json")
//            }
//        }

        dependencies {
            dependency("io.springfox:springfox-swagger-ui:${swagger2Version}")
            dependency("io.springfox:springfox-swagger2:${swagger2Version}")
            dependency("com.google.guava:guava:${guavaVersion}")
            dependency("org.mybatis.spring.boot:mybatis-spring-boot-starter:${mybatisVersion}")
            dependency("org.mybatis.spring.boot:mybatis-spring-boot-starter:${mybatisVersion}")
            dependency("com.alibaba:fastjson:${fastjsonVersion}")
            dependency("mysql:mysql-connector-java:${mysqlVersion}")
//            dependency('org.springframework:spring-core:4.0.3.RELEASE') {
//                exclude 'commons-logging:commons-logging'
//            }
        }
    }

    dependencies {
        implementation(kotlin("stdlib-jdk8"))
        implementation(kotlin("reflect"))
        implementation("com.alibaba:fastjson")

        implementation("org.projectlombok:lombok")
        annotationProcessor("org.projectlombok:lombok")

        testImplementation(kotlin("test"))
        testImplementation(kotlin("test-junit"))
        testImplementation("org.springframework.boot:spring-boot-starter-test")
    }
}
