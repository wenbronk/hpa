
dependencies {

    implementation("org.springframework.boot", "spring-boot-starter-web")
    implementation("org.springframework.boot", "spring-boot-configuration-processor")
    compile("io.springfox", "springfox-swagger-ui")
    compile("io.springfox", "springfox-swagger2")
}

