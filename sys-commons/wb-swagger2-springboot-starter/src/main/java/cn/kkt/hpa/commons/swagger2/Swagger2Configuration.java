package cn.kkt.hpa.commons.swagger2;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import springfox.documentation.swagger2.configuration.Swagger2DocumentationConfiguration;

/**
 * @Author wenbronk <meng.wen@kangtaitong.cn>
 * @Date 2019-10-08 09:36
 * description:
 */
@Configuration
@ConditionalOnProperty(name = "ktt.swagger2.enabled", matchIfMissing = true)
@Import({Swagger2DocumentationConfiguration.class})
@EnableSwagger2
public class Swagger2Configuration {

    @Bean
    @ConditionalOnMissingBean
    public SwaggerProperties swaggerProperties() {
        return new SwaggerProperties();
    }

}
