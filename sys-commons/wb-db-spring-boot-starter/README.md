## 1. jpa软删除使用
1.1)  @EnableJpaSoftDeleteRepositories, 注解在启动主类上 

1.2) 实体类包含属性
```java
    @Column(name = "delete_time", columnDefinition = "datetime DEFAULT NULL COMMENT '删除时间'")
    protected Date deleteTime;
```

1.3) 不想使用软删除的repository注解
`@HardDelete`

1.4) 软删除的更新中， 会忽略对 软删除判断字段的更新， 即使用更新的方法无法操作软删除字段