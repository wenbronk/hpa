package cn.ktt.hpa.commons.db.jpa.softdelete.support;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.provider.PersistenceProvider;
import org.springframework.data.jpa.repository.query.EscapeCharacter;
import org.springframework.data.jpa.repository.support.CrudMethodMetadata;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.JpaEntityInformationSupport;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.lang.Nullable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.beans.PropertyDescriptor;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

import static org.springframework.data.jpa.repository.query.QueryUtils.getQueryString;

/**
 * @Author wenbronk <meng.wen@kangtaitong.cn>
 * @Date 2019-10-10 09:15
 * description:
 */
@Transactional(readOnly = true)
public class JpaSoftDeleteRepository<T, ID extends Serializable> extends SimpleJpaRepository<T, ID> {

    private static final String ID_MUST_NOT_BE_NULL = "The given id must not be null!";
    private static final String ENTITY_MUST_NOT_BE_NULL = "The given entity must not be null!";
    private static final String SOFT_DELETE_FLAG_COLUMN = "delete_time";
    private static final String SOFT_DELETE_FLAG_PROPERTIES = "deleteTime";

    private final JpaEntityInformation<T, ?> entityInformation;
    private final EntityManager em;
    private final PersistenceProvider provider;

    private @Nullable
    CrudMethodMetadata metadata;
    private EscapeCharacter escapeCharacter = EscapeCharacter.DEFAULT;

    /**
     * Creates a new {@link JpaSoftDeleteRepository} to manage objects of the given {@link JpaEntityInformation}.
     *
     * @param entityInformation must not be {@literal null}.
     * @param entityManager     must not be {@literal null}.
     */
    public JpaSoftDeleteRepository(JpaEntityInformation<T, ?> entityInformation, EntityManager entityManager) {
        super(entityInformation, entityManager);
        this.entityInformation = entityInformation;
        this.em = entityManager;
        this.provider = PersistenceProvider.fromEntityManager(entityManager);
    }


    /**
     * Creates a new {@link JpaSoftDeleteRepository} to manage objects of the given domain type.
     *
     * @param domainClass must not be {@literal null}.
     * @param em          must not be {@literal null}.
     */
    public JpaSoftDeleteRepository(Class<T> domainClass, EntityManager em) {
        this(JpaEntityInformationSupport.getEntityInformation(domainClass, em), em);
    }

    @Override
    public void setRepositoryMethodMetadata(CrudMethodMetadata crudMethodMetadata) {
        super.setRepositoryMethodMetadata(crudMethodMetadata);
        this.metadata = crudMethodMetadata;
    }

    private String getSoftDeleteAllQueryString() {
        String softDeleteAllQueryString = new StringBuilder()
                .append("UPDATE %s x ")
                .append("SET ")
                .append(SOFT_DELETE_FLAG_PROPERTIES)
                .append("=")
                .append(" :")
                .append(SOFT_DELETE_FLAG_PROPERTIES)
                .toString();
        return getQueryString(softDeleteAllQueryString, entityInformation.getEntityName());
    }

    @Override
    @Transactional
    public void delete(T entity) {
        Assert.notNull(entity, ENTITY_MUST_NOT_BE_NULL);
        softDelete(entity, new Date());
    }

    private void softDelete(T entity, Date date) {
        Assert.notNull(entity, ENTITY_MUST_NOT_BE_NULL);
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaUpdate<T> updater = cb.createCriteriaUpdate(getDomainClass());
        Root<T> root = updater.from(getDomainClass());
        updater.set(SOFT_DELETE_FLAG_PROPERTIES, date);

        final List<Predicate> predicates = new ArrayList<>();
        if (entityInformation.hasCompositeId()) {
            entityInformation.getIdAttributeNames().forEach(idName -> {
                predicates.add(cb.equal(root.get(idName),
                        entityInformation.getCompositeIdAttributeValue(entityInformation.getId(entity), idName)));
            });
            updater.where(cb.and(predicates.toArray(new Predicate[predicates.size()])));
        } else {
            updater.where(cb.equal(root.get(entityInformation.getIdAttribute().getName()), entityInformation.getId(entity)));
        }
        em.createQuery(updater).executeUpdate();
    }


    @Override
    @Transactional
    public void deleteInBatch(Iterable<T> entities) {
        Assert.notNull(entities, "Entities must not be null!");

        if (!entities.iterator().hasNext()) {
            return;
        }
        StringBuilder whereBuilder = new StringBuilder(getSoftDeleteAllQueryString());

        Iterator<T> iterator = entities.iterator();
        int i = 0;

        whereBuilder.append(" where x in (:entities)");

        Query query = em.createQuery(whereBuilder.toString());

        query.setParameter(SOFT_DELETE_FLAG_PROPERTIES, new Date());
        query.setParameter("entities", entities);
        query.executeUpdate();
    }

    @Override
    @Transactional
    public void deleteAllInBatch() {
        em.createQuery(getSoftDeleteAllQueryString())
                .setParameter(SOFT_DELETE_FLAG_PROPERTIES, new Date())
                .executeUpdate();
    }

    @Override
    @Transactional
    public Optional<T> findById(ID id) {
        Assert.notNull(id, ID_MUST_NOT_BE_NULL);
        return super.findOne(Specification.where(new ByIdSpecification<T, ID>(id, entityInformation)));
    }

    @Override
    public T getOne(ID id) {
        return this.findById(id).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public boolean existsById(ID id) {
        return this.findById(id).isPresent();
    }


    @Override
    protected <S extends T> TypedQuery<S> getQuery(Specification<S> spec, Class<S> domainClass, Sort sort) {
        return super.getQuery(spec != null ? spec.and(notDeleted()) : notDeleted(), domainClass, sort);
    }

    @Override
    public long count() {
        return super.count(notDeleted());
    }

    @Override
    public <S extends T> S save(S entity) {
        if (entityInformation.isNew(entity)) {
            // TODO 防止前端传入 删除标记
            em.persist(entity);
            return entity;
        } else {
            ID entityId = (ID) entityInformation.getId(entity);
            Optional<T> optionalT;
            optionalT = super.findById(entityId);
            if (optionalT.isPresent()) {
                String[] nullProperties = getNullProperties(entity);
                T target = optionalT.get();
                BeanUtils.copyProperties(entity, target, nullProperties);
                em.merge(target);
            } else {
                em.merge(entity);
            }
            return entity;
        }
    }

    private String[] getNullProperties(Object source) {
        final BeanWrapper srcBean = new BeanWrapperImpl(source);
        PropertyDescriptor[] pds = srcBean.getPropertyDescriptors();

        return Arrays.stream(pds).filter(pd ->
                SOFT_DELETE_FLAG_PROPERTIES.equals(pd.getName()) || srcBean.getPropertyValue(pd.getName()) == null)
                .map(PropertyDescriptor::getName).distinct().toArray(String[]::new);
    }

    @Override
    protected <S extends T> TypedQuery<Long> getCountQuery(Specification<S> spec, Class<S> domainClass) {
        return super.getCountQuery(spec != null ? spec.and(notDeleted()) : notDeleted(), domainClass);
    }

    private static final class ByIdSpecification<T, ID extends Serializable> implements Specification<T> {
        private static final long serialVersionUID = 1L;

        private final ID id;
        private final JpaEntityInformation<T, ?> information;

        public ByIdSpecification(ID id, JpaEntityInformation<T, ?> information) {
            this.id = id;
            this.information = information;
        }

        @Override
        public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
            final List<Predicate> predicates = new ArrayList<>();
            if (information.hasCompositeId()) {
                information.getIdAttributeNames().forEach(name ->
                        predicates.add(criteriaBuilder.equal(root.get(name), information.getCompositeIdAttributeValue(id, name))));
                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
            return criteriaBuilder.equal(root.get(information.getIdAttribute().getName()), id);
        }
    }

    private static final class DeletedIsNull<T> implements Specification<T> {
        @Override
        public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
            return cb.isNull(root.<Date>get(SOFT_DELETE_FLAG_PROPERTIES));
        }
    }

    private static final class DeletedTimeGreatherThanNow<T> implements Specification<T> {
        @Override
        public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
            return cb.greaterThan(root.<Date>get(SOFT_DELETE_FLAG_PROPERTIES), new Date());
        }
    }

    private static final <T> Specification<T> notDeleted() {
        return Specification.where(new DeletedIsNull<T>()).or(new DeletedTimeGreatherThanNow<T>());
    }

}