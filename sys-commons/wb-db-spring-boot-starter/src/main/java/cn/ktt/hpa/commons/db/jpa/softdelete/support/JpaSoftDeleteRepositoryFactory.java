package cn.ktt.hpa.commons.db.jpa.softdelete.support;

import cn.ktt.hpa.commons.db.jpa.softdelete.HardDelete;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactory;
import org.springframework.data.repository.core.RepositoryMetadata;

import javax.persistence.EntityManager;

/**
 * @Author wenbronk <meng.wen@kangtaitong.cn>
 * @Date 2019-10-10 09:12
 * description:
 */
public class JpaSoftDeleteRepositoryFactory extends JpaRepositoryFactory {
    /**
     * Creates a new {@link JpaRepositoryFactory}.
     *
     * @param entityManager must not be {@literal null}
     */
    public JpaSoftDeleteRepositoryFactory(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    protected Class<?> getRepositoryBaseClass(RepositoryMetadata metadata) {

        if(metadata.getRepositoryInterface().isAnnotationPresent(HardDelete.class)){
            return super.getRepositoryBaseClass(metadata);
        }
        return JpaSoftDeleteRepository.class;
    }
}
