dependencies {
    implementation(project(":sys-commons:wb-db-spring-boot-starter"))
    implementation("com.esotericsoftware.reflectasm:reflectasm:1.05")
    implementation("org.apache.commons:commons-lang3:3.9")
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation("io.springfox", "springfox-swagger-ui")
    implementation("io.springfox", "springfox-swagger2")
}