package cn.ktt.hpa.commons.entity.form;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Author wenbronk <meng.wen@kangtaitong.cn>
 * @Date 2019-10-17 13:55
 * description:
 */
@Data
public abstract class AbstractForm implements FormTransfor {

}
