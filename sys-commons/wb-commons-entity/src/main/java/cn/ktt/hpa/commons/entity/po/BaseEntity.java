package cn.ktt.hpa.commons.entity.po;


import cn.ktt.hpa.commons.entity.Transfor;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

/**
 * @Author wenbronk <meng.wen@kangtaitong.cn>
 * @Date 2019-10-17 15:46
 * description:
 */
@Data
@MappedSuperclass
@EqualsAndHashCode(callSuper = true)
public abstract class BaseEntity extends IdEntity implements POTransfor {
    private static final long serialVersionUID = 1L;

//    protected static final String NOT_DELETED = "delete_time > CURRENT_TIMESTAMP OR delete_time IS NULL";
    protected static final String NOT_DELETED = "delete_time > CURRENT_TIMESTAMP() OR delete_time IS NULL";

    @Temporal(TemporalType.TIMESTAMP)
    @Column(columnDefinition = "timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'")
    protected Date createTime;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(columnDefinition = "timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间'")
    protected Date updateTime;

    @Column(name = "delete_time", columnDefinition = "datetime DEFAULT NULL COMMENT '删除时间'")
    protected Date deleteTime;

    @Column(columnDefinition = "char(36) COMMENT '租户id'")
    protected String tenantId;

//    @Version
//    protected Long version;

    @PrePersist
    protected void onCreate() {
        this.createTime = new Date();
        this.updateTime = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
        this.updateTime = new Date();
    }

}