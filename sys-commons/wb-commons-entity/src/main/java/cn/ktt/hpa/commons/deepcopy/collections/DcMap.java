package cn.ktt.hpa.commons.deepcopy.collections;


import cn.ktt.hpa.commons.deepcopy.Dco;

import java.util.Map;

/**
 * @author LiJunhua <junhua.li@kangtaitong.cn>
 * @version 1.0.0
 * create_time    2019-05-14 2:42
 * description
 */
public interface DcMap<K,V extends Dco> extends Map<K,V>,CollectionCommon {
}