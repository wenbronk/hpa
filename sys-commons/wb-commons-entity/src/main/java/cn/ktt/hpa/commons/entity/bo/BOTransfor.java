package cn.ktt.hpa.commons.entity.bo;

import cn.ktt.hpa.commons.deepcopy.Dco;
import cn.ktt.hpa.commons.entity.Transfor;
import cn.ktt.hpa.commons.entity.dto.AbstractDTO;
import cn.ktt.hpa.commons.entity.TransforException;
import cn.ktt.hpa.commons.entity.form.AbstractForm;
import cn.ktt.hpa.commons.entity.po.BaseEntity;
import cn.ktt.hpa.commons.entity.vo.AbstractVO;

import java.util.function.Supplier;

/**
 * @Author wenbronk <meng.wen@kangtaitong.cn>
 * @Date 2019-10-17 14:14
 * description:
 */
public interface BOTransfor extends Transfor, Dco {

    default  <T extends BaseEntity> T toPO(Class<T> clazz) throws TransforException {
        return transfor(clazz);
    }

    default <FORM extends AbstractForm> FORM toForm(Class<FORM> clazz) throws TransforException {
        return transfor(clazz);
    }

    default <VO extends AbstractVO> VO toVO(Class<VO> clazz) throws TransforException {
        return transfor(clazz);
    }

    default <DTO extends AbstractDTO> DTO toDTO(Class<DTO> clazz) throws TransforException {
        return transfor(clazz);
    }

}
