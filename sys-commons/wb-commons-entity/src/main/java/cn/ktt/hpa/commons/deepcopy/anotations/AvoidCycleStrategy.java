package cn.ktt.hpa.commons.deepcopy.anotations;

/**
 * @author LiJunhua <junhua.li@kangtaitong.cn>
 * @version 1.0.0
 * create_time    2019-05-15 23:39
 * description
 */
public enum AvoidCycleStrategy {
    AUTO("AUTO"),MANUAL("MANUAL");
    private String value;
    AvoidCycleStrategy(String value){
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}

