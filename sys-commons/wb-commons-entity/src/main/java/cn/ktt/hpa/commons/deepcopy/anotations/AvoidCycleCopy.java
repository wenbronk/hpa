package cn.ktt.hpa.commons.deepcopy.anotations;

import java.lang.annotation.*;

/**
 * @author LiJunhua <junhua.li@kangtaitong.cn>
 * @version 1.0.0
 * create_time    2019-05-15 18:35
 * description
 */
@Documented
@Inherited
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface AvoidCycleCopy {
    AvoidCycleStrategy value() default AvoidCycleStrategy.MANUAL;
}
