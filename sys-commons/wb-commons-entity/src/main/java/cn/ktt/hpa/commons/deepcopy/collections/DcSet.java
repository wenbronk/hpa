package cn.ktt.hpa.commons.deepcopy.collections;


import cn.ktt.hpa.commons.deepcopy.Dco;
import java.util.Set;

/**
 * @author LiJunhua <junhua.li@kangtaitong.cn>
 * @version 1.0.0
 * create_time    2019-05-15 18:02
 * description
 */
public interface DcSet<E extends Dco> extends Set<E>,CollectionCommon {
}