package cn.ktt.hpa.commons.deepcopy.anotations;

import java.lang.annotation.*;

/**
 * @author LiJunhua <junhua.li@kangtaitong.cn>
 * @version 1.0.0
 * create_time    2019-05-14 15:14
 * description
 */
@Documented
@Target({ElementType.FIELD,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface IgnoreToCopy {
}
