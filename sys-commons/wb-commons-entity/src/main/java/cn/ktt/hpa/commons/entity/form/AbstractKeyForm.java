package cn.ktt.hpa.commons.entity.form;

import cn.ktt.hpa.commons.entity.IdInterface;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public abstract class AbstractKeyForm extends AbstractForm implements IdInterface {

    protected Long id;
}
