package cn.ktt.hpa.commons.exception;

import cn.ktt.hpa.commons.response.BaseResponse;
import cn.ktt.hpa.commons.response.ResultCode;

/**
 * @Author wenbronk <meng.wen@kangtaitong.cn>
 * @Date 2019-10-17 13:21
 * description:
 */
public class ExceptionWrapper extends BaseResponse {

    public ExceptionWrapper() {
        super();
    }

    public ExceptionWrapper(ResultCode resultCode) {
        super(resultCode);
    }

    public ExceptionWrapper(ResultCode resultCode, Object data) {
        super(resultCode, data);
    }
}
