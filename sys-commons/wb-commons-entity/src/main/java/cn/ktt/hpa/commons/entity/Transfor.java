package cn.ktt.hpa.commons.entity;

import cn.ktt.hpa.commons.deepcopy.BeanDeepCopyUtils;
import cn.ktt.hpa.commons.entity.code.EntityCode;
import org.springframework.beans.BeanUtils;

import java.util.Arrays;
import java.util.function.Supplier;
import java.util.stream.Stream;

/**
 * @Author wenbronk <meng.wen@kangtaitong.cn>
 * @Date 2019-10-17 11:15
 * description:
 */
public interface Transfor {

    /**
     * 忽略id， tenantId属性的复制
     */
//    public static final String[] IGNORE_PROPERTIES = {"deleteTime", "tenantId"};
    public static final String[] IGNORE_PROPERTIES = {"deleteTime"};

    /**
     * deep copy
     * @param clazz
     * @param <S>
     * @return
     */
    default <S> S transfor(Class<S> clazz) {
        return BeanDeepCopyUtils.deepCopy(this,clazz, IGNORE_PROPERTIES);
    }

    default <S> S transfor(Supplier<S> supplier) throws TransforException {
        return this.transfor(supplier, (String[]) null);
    }

    default <S> S transfor(Supplier<S> supplier, String... ignoreProperties) throws TransforException {
        if (ignoreProperties == null || ignoreProperties.length == 0) {
            ignoreProperties = IGNORE_PROPERTIES;
        } else {
            ignoreProperties = Stream.concat(Arrays.stream(ignoreProperties), Arrays.stream(IGNORE_PROPERTIES)).toArray(String[]::new);
        }

        S s = supplier.get();
        try {
            this.copyProperties(this, s, ignoreProperties);
            return s;
        } catch (Exception e) {
            String message = String.format("%s transfor to %s error", this.getClass().getName(), s == null ? null : supplier.getClass());
            throw new TransforException(EntityCode.TRANSFOR_ERROR, message, e);
        }
    }


    default <THIS, THAT> THAT copyProperties(THIS src, THAT desc) {
        BeanUtils.copyProperties(this, desc);
        return desc;
    }

    default <THIS, THAT> THAT copyProperties(THIS src, THAT desc, String... ignoreProperties) {
        BeanUtils.copyProperties(this, desc, ignoreProperties);
        return desc;
    }

}
