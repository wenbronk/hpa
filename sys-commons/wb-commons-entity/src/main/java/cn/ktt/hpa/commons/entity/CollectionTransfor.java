package cn.ktt.hpa.commons.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author lei.yan
 * @Date 2019/11/5 15:54
 * @Version 1.0
 * description:
 */
public class CollectionTransfor {

    public static <S extends Transfor, D extends Transfor> List<D> convert(List<S> transfors, Class<D> clazz) throws TransforException {

        List<D> list = new ArrayList<>();
        for (S t : transfors) {
            list.add(t.transfor(clazz));
        }
        return list;
    }
}
