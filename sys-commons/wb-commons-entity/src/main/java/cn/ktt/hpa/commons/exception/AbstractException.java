package cn.ktt.hpa.commons.exception;

import cn.ktt.hpa.commons.response.CommonsCode;
import cn.ktt.hpa.commons.response.ResultCode;
import org.springframework.cache.annotation.Cacheable;

import java.io.PrintStream;
import java.io.PrintWriter;

/**
 * @Author wenbronk <meng.wen@kangtaitong.cn>
 * @Date 2019-10-17 11:32
 * description:
 */
public abstract class AbstractException extends Exception {
    private static final long serialVersionUID = 5082634801360427800L;

    protected Throwable cause;

    private ExceptionWrapper wrapper = new ExceptionWrapper();

    public AbstractException(String message) {
        this(CommonsCode.SERVER_ERROR, message, null);
    }

    public AbstractException(Throwable cause) {
        this(CommonsCode.SERVER_ERROR, cause);
    }

    public AbstractException(ResultCode resultCode) {
        this(resultCode, null);
    }

    public AbstractException(ResultCode resultCode, Throwable cause) {
        this(resultCode, resultCode.message(), cause);
    }

    public AbstractException(ResultCode resultCode, String message, Throwable cause) {
        super(message, cause);
        this.wrapper.setCode(resultCode.code());
        this.wrapper.setMessage(message);
        this.cause = cause;
    }

    @Override
    public Throwable getCause() {
        return cause;
    }

    @Override
    public void printStackTrace() {
        super.printStackTrace();
        if (cause != null) {
            cause.printStackTrace();
        }
    }

    @Override
    public void printStackTrace(PrintStream s) {
        super.printStackTrace(s);
        if (cause != null) {
            cause.printStackTrace(s);
        }
    }

    @Override
    public void printStackTrace(PrintWriter s) {
        super.printStackTrace(s);
        if (cause != null) {
            cause.printStackTrace(s);
        }
    }

}
