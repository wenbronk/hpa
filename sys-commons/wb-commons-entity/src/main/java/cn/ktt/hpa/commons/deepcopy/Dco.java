package cn.ktt.hpa.commons.deepcopy;

import java.util.ArrayList;
import java.util.List;

/**
 * @author LiJunhua <junhua.li@kangtaitong.cn>
 * @version 1.0.0
 * create_time    2019-05-14 2:20
 * description
 */
public interface Dco {
    ThreadLocal<List<Dco>> callLayers = ThreadLocal.withInitial(ArrayList::new);

    default boolean addCallLayer(Dco dco) {
        boolean result;

        List<Dco> cl = callLayers.get();

        if (cl.contains(dco)) {
            result = false;
        }else {
            cl.add(dco);
            BeanDeepCopyUtils.addObject(this);
            result = true;
        }

        return result;
    }

    default void clearCallLayers() {
        callLayers.get().clear();
    }
}