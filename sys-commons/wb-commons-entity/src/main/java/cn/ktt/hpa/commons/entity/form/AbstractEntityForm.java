package cn.ktt.hpa.commons.entity.form;


import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Data
public abstract class AbstractEntityForm extends AbstractKeyForm {

}
