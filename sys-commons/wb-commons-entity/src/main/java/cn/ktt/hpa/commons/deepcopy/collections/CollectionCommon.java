package cn.ktt.hpa.commons.deepcopy.collections;

/**
 * @author LiJunhua <junhua.li@kangtaitong.cn>
 * @version 1.0.0
 * create_time    2019-05-15 19:44
 * description
 */
public interface CollectionCommon {
    Class<?> getRealType();
}
