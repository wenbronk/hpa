package cn.ktt.hpa.commons.entity.vo;

import cn.ktt.hpa.commons.deepcopy.Dco;
import cn.ktt.hpa.commons.entity.IdInterface;
import cn.ktt.hpa.commons.entity.Transfor;
import lombok.Data;

/**
 * @Author lei.yan
 * @Date 2019/11/5 16:56
 * @Version 1.0
 * description:
 */
@Data
public abstract class AbstractVO implements IdInterface, VOTransfor {
    protected Long id;
}
