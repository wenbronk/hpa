package cn.ktt.hpa.commons.deepcopy.collections;


import cn.ktt.hpa.commons.deepcopy.Dco;

import java.util.HashMap;

/**
 * @author LiJunhua <junhua.li@kangtaitong.cn>
 * @version 1.0.0
 * create_time    2019-05-14 2:49
 * description
 */
public class DcHashMap<K,V extends Dco> extends HashMap<K,V> implements DcMap<K,V> {
    private final Class<V> clazz;

    public DcHashMap(Class<V> clazz){
        this.clazz = clazz;
    }

    @Override
    public Class<?> getRealType(){
        return clazz;
    }
}