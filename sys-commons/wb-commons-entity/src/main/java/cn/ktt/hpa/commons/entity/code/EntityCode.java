package cn.ktt.hpa.commons.entity.code;

import cn.ktt.hpa.commons.response.ResultCode;

/**
 * @Author wenbronk <meng.wen@kangtaitong.cn>
 * @Date 2019-10-17 13:37
 * description:
 */
public enum EntityCode implements ResultCode {

    /**
     *
     */
    TRANSFOR_ERROR(false, 11000, "entity转换错误")
    ;

    private boolean success;
    private int code;
    private String message;

    private EntityCode(boolean success, int code, String message) {
        this.success = success;
        this.code = code;
        this.message = message;
    }


    @Override
    public boolean success() {
        return this.success;
    }

    @Override
    public int code() {
        return this.code;
    }

    @Override
    public String message() {
        return this.message;
    }
}
