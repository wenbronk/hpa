package cn.ktt.hpa.commons.entity.form;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public abstract class AbstractPageForm extends AbstractForm {

    protected int pageIndex = 0;
    protected int pageSize = 20;
}
