package cn.ktt.hpa.commons.entity.vo;

import cn.ktt.hpa.commons.deepcopy.Dco;
import cn.ktt.hpa.commons.entity.Transfor;

/**
 * @Author lei.yan
 * @Date 2019/11/5 16:56
 * @Version 1.0
 * description:
 */
public interface VOTransfor extends Dco, Transfor {
}
