package cn.ktt.hpa.commons.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @Author wenbronk <meng.wen@kangtaitong.cn>
 * @Date 2019-10-17 09:28
 * description:
 */
@Data
@ToString
@NoArgsConstructor
@ApiModel(value = "统一返回对象")
public class BaseResponse {
//public class BaseResponse<T> {

    @ApiModelProperty(name = "是否执行成功")
    private boolean success;

    @ApiModelProperty(name = "返回状态码")
    private int code;

    @ApiModelProperty(name = "返回信息")
    private String message;

    @ApiModelProperty(name = "返回值")
    private Object data;

    public BaseResponse(ResultCode resultCode) {
        this(resultCode, null);
    }

    public BaseResponse(ResultCode resultCode, Object data) {
        this.success = resultCode.success();
        this.code = resultCode.code();
        this.message = resultCode.message();
        this.data = data;
    }

    public static BaseResponse getResponse(ResultCode resultCode) {
        return new BaseResponse(resultCode);
    }

    public static BaseResponse getResponse(ResultCode resultCode, Object data) {
        return new BaseResponse(resultCode, data);
    }

    public static BaseResponse SUCCESS() {
        return new BaseResponse(CommonsCode.SUCCESS);
    }

}
