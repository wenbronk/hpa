package cn.ktt.hpa.commons.entity;

public interface PageInterface {

    int getPageIndex();

    void setPageIndex(int pageIndex);

    int getPageSize();

    void setPageSize(int pageSize);
}
