package cn.ktt.hpa.commons.entity.po;

import cn.ktt.hpa.commons.entity.IdInterface;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.lang.annotation.Annotation;

/**
 * @Author wenbronk <meng.wen@kangtaitong.cn>
 * @Date 2019-10-17 10:44
 * description:
 */
@Data
@MappedSuperclass
@EqualsAndHashCode
public abstract class IdEntity implements Serializable, IdInterface {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", columnDefinition = "bigint(36) COMMENT 'id'")
    @GenericGenerator(name = "snowFlakedId", strategy = "cn.ktt.hpa.commons.db.id.SnowflakeIdWorker")
    @GeneratedValue(generator = "snowFlakedId", strategy = GenerationType.AUTO)
    @ApiModelProperty(value = "id", example = "632523426514472963L")
    protected Long id;

}