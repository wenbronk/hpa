package cn.ktt.hpa.commons.deepcopy;

import cn.ktt.hpa.commons.deepcopy.anotations.*;
import cn.ktt.hpa.commons.deepcopy.collections.CollectionCommon;
import cn.ktt.hpa.commons.deepcopy.collections.DcList;
import cn.ktt.hpa.commons.deepcopy.collections.DcMap;
import cn.ktt.hpa.commons.deepcopy.collections.DcSet;
import com.esotericsoftware.reflectasm.MethodAccess;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.*;

/**
 * @author LiJunhua <junhua.li@kangtaitong.cn>
 * @version 1.0.0
 * create_time    2019-05-13 19:37
 * description
 */
public class BeanDeepCopyUtils {
    private static final ThreadLocal<List<Dco>> clearObjects = ThreadLocal.withInitial(ArrayList::new);

    private static final ThreadLocal<Integer> count = ThreadLocal.withInitial(() -> 0);

    private static final Map<Class, MethodAccess> methodMap = new HashMap<>();

    private static final Map<String, Integer> methodIndexMap = new HashMap<>();

    private static final Map<Class, TreeMap<String, Field>> fieldMap = new HashMap<>();//String为首字母大写的fieldName

    public static void addObject(Dco dco) {
        clearObjects.get().add(dco);
    }

    private static void clearObject() {
        try {
            List<Dco> dcos = clearObjects.get();
            for (Dco dco : dcos) {
                dco.clearCallLayers();
            }
            dcos.clear();
        } catch (Exception e) {
            //
        }
    }

    public static <T> T deepCopy(Object src, Class<T> dstClazz, String... ignoreProperties) {
        if (src == null) return null;

        Class srcClazz = src.getClass();

        boolean isAlwaysContinue = isAlwaysContinue(srcClazz, dstClazz);

        String srcClassName = srcClazz.getName();
        String dstClassName = dstClazz.getName();

        MethodAccess sourceMethodAccess = methodMap.get(srcClazz);
        if (sourceMethodAccess == null) {
            sourceMethodAccess = cache(srcClazz, ignoreProperties);
        }

        MethodAccess dstMethodAccess = methodMap.get(dstClazz);
        if (dstMethodAccess == null) {
            dstMethodAccess = cache(dstClazz, ignoreProperties);
        }

        T dst = null;

        try {
            count.set(count.get() + 1);
            dst = (T) dstClazz.newInstance(); //创建目标对象实例

            TreeMap<String, Field> srcTreeMap = fieldMap.get(srcClazz);
            TreeMap<String, Field> dstTreeMap = fieldMap.get(dstClazz);

            for (Map.Entry<String, Field> sEntry : srcTreeMap.entrySet()) { //遍历源所有属性
                Field sf = sEntry.getValue();
                String fieldCapitalName = sEntry.getKey();

                Field df = dstTreeMap.get(sEntry.getKey());

                if (df == null
                        || sf.isAnnotationPresent(IgnoreToCopy.class)
                        || df.isAnnotationPresent(IgnoreToCopy.class)
                        ) continue;


                String srcGetKey = srcClassName + "." + "get" + fieldCapitalName;
                int srcGetIndex = -1;
                if (methodIndexMap.containsKey(srcGetKey)) {
                    srcGetIndex = methodIndexMap.get(srcGetKey);
                }
                if (srcGetIndex < 0) continue;

                String dstGetKey = dstClassName + "." + "get" + fieldCapitalName;
                String dstSetKey = dstClassName + "." + "set" + fieldCapitalName;
                int dstGetIndex = -1;
                if (methodIndexMap.containsKey(dstGetKey)) {
                    dstGetIndex = methodIndexMap.get(dstGetKey);
                }

                int dstSetIndex = -1;
                if (methodIndexMap.containsKey(dstSetKey)) {
                    dstSetIndex = methodIndexMap.get(dstSetKey);
                }
                if (dstSetIndex < 0) continue;

                if (fieldCapitalName.equals(fieldCapitalName)) { //属性名字相同
                    Object d;
                    Object s = sourceMethodAccess.invoke(src, srcGetIndex);
                    Class<?> dType = df.getType();

                    if (isPrimitive(s)) {
                        dstMethodAccess.invoke(dst, dstSetIndex, s);
                    } else {
                        if (dstGetIndex < 0) continue;
                        d = dstMethodAccess.invoke(dst, dstGetIndex);
                        if (s instanceof Dco) {//dco
                            if (isAlwaysContinue || isContinue(src, s)) {
                                d = deepCopy(s, dType);
                            }
                            dstMethodAccess.invoke(dst, dstSetIndex, d);
                        } else {
                            //collection 解决集合或字典类型不一致情况
                            s = checkTypeCopyAndConvert(s,sf);

                            if (s instanceof DcList) {
                                doDcCollection((DcList) d, (DcList) s, dType, src, dstMethodAccess, dst, dstSetIndex, isAlwaysContinue);
                            } else if (s instanceof DcSet) {
                                doDcCollection((DcSet) d, (DcSet) s, dType, src, dstMethodAccess, dst, dstSetIndex, isAlwaysContinue);
                            } else if (s instanceof DcMap) {
                                doDcMap((DcMap) d, (DcMap) s, dType, src, dstMethodAccess, dst, dstSetIndex, isAlwaysContinue);
                            } else if (s instanceof Collection) {
                                doCollection((Collection) d, (Collection) s, dType, src, dstMethodAccess, dst, dstSetIndex, isAlwaysContinue);
                            } else if (s instanceof Map) {
                                doMap((Map) d, (Map) s, dType, src, dstMethodAccess, dst, dstSetIndex, isAlwaysContinue);
                            } else {
                                if (isAlwaysContinue || isContinue(src, s)) {
                                    dstMethodAccess.invoke(dst, dstSetIndex, s);
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            count.set(count.get() - 1);
        }

        if (count.get() == 0) {
            clearObject();
        }
        return dst;
    }

    private static Object checkTypeCopyAndConvert(Object s,Field sf){

        if(s instanceof Collection){
            CollectionTypeCopy collectionTypeCopy = sf.getAnnotation(CollectionTypeCopy.class);
            if(collectionTypeCopy != null){
                try{
                    Class<?> ct = collectionTypeCopy.value();
                    Class<?> elementType = collectionTypeCopy.elementType();
                    Collection ss = (Collection) s;
                    Constructor constructor = ct.getDeclaredConstructor(Class.class);
                    s = constructor.newInstance(elementType);
                    ((Collection) s).addAll(ss);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
        else if(s instanceof Map){
            MapTypeCopy mapTypeCopy = sf.getAnnotation(MapTypeCopy.class);
            if(mapTypeCopy != null){
                try{
                    Class<?> ct = mapTypeCopy.value();
                    Class<?> entryValueType = mapTypeCopy.entryValueType();
                    Map ss = (Map) s;
                    Constructor constructor = ct.getDeclaredConstructor(Class.class);
                    s = constructor.newInstance(entryValueType);
                    ((Map) s).putAll(ss);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }

        return s;
    }

    private static boolean isAlwaysContinue(Class<?> srcClazz, Class<?> dstClazz) {
        AvoidCycleStrategy strategy = AvoidCycleStrategy.AUTO;

        AvoidCycleCopy avoidCycleCopy = (AvoidCycleCopy) srcClazz.getAnnotation(AvoidCycleCopy.class);
        if (avoidCycleCopy != null && avoidCycleCopy.value() == AvoidCycleStrategy.MANUAL) {
            strategy = AvoidCycleStrategy.MANUAL;
        } else {
            avoidCycleCopy = (AvoidCycleCopy) dstClazz.getAnnotation(AvoidCycleCopy.class);
            if (avoidCycleCopy != null && avoidCycleCopy.value() == AvoidCycleStrategy.MANUAL) {
                strategy = AvoidCycleStrategy.MANUAL;
            }
        }
        if (strategy == AvoidCycleStrategy.MANUAL) {
            return true;
        } else {
            return false;
        }
    }

    private static <T> void doDcCollection(Collection d, Collection s, Class<?> dType, Object src, MethodAccess dstMethodAccess, T dst, int dstSetIndex, boolean isAlwaysContinue) throws Exception {
        if (s == null) return;
        if (d == null) {
            if (!s.isEmpty()) {
                Constructor<?> constructor = s.getClass().getConstructor(Class.class);
                if (s instanceof DcList) {

                    d = (DcList) constructor.newInstance(((CollectionCommon) s).getRealType());
                } else if (s instanceof DcSet) {
                    d = (DcSet) constructor.newInstance(((CollectionCommon) s).getRealType());
                }

                dstMethodAccess.invoke(dst, dstSetIndex, d);
            }
        } else {
            d.clear();
        }

        for (Object dmo : s) {
            if (isAlwaysContinue || isContinue(src, s)) {
                d.add(deepCopy(dmo, ((CollectionCommon) d).getRealType()));
            }
        }
    }

    private static <T> void doDcMap(DcMap d, DcMap s, Class<?> dType, Object src, MethodAccess dstMethodAccess, T dst, int dstSetIndex, boolean isAlwaysContinue) throws Exception {
        if (s == null) return;
        if (d == null) {
            if (!s.isEmpty()) {
                Constructor<?> constructor = s.getClass().getConstructors()[0];
                d = (DcMap) constructor.newInstance(((CollectionCommon) s).getRealType());

                Iterator entries = s.entrySet().iterator();
                while (entries.hasNext()) {
                    Map.Entry entry = (Map.Entry) entries.next();
                    if (isAlwaysContinue || isContinue(src, s)) {
                        d.put(entry.getKey(), deepCopy(entry, ((CollectionCommon) d).getRealType()));
                    }
                }
                dstMethodAccess.invoke(dst, dstSetIndex, d);
            }
        } else {
            if (isAlwaysContinue || isContinue(src, s)) {
                d.clear();
                d.putAll(s);
            }
        }
    }

    private static <T> void doCollection(Collection d, Collection s, Class<?> dType, Object src, MethodAccess dstMethodAccess, T dst, int dstSetIndex, boolean isAlwaysContinue) throws IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchMethodException {
        if (s == null) return;
        if (d == null) {
            if (!s.isEmpty()) {
//                Constructor constructor = s.getClass().getDeclaredConstructors()[0];
//                constructor.setAccessible(true);
//                d = (Collection)constructor.newInstance();
                if (isAlwaysContinue || isContinue(src, s)) {
                    if (s instanceof List) {
                        d = new ArrayList<>();
                    } else if (s instanceof Set) {
                        d = new HashSet<>();
                    }
                    d.addAll(s);
                }
                dstMethodAccess.invoke(dst, dstSetIndex, d);
            }
        } else {
            if (isAlwaysContinue || isContinue(src, s)) {
                d.clear();
                d.addAll(s);
            }
        }
    }

    private static <T> void doMap(Map d, Map s, Class<?> dType, Object src, MethodAccess dstMethodAccess, T dst, int dstSetIndex, boolean isAlwaysContinue) {
        if (s == null) return;
        if (d == null) {
            if (!s.isEmpty()) {

                if (isAlwaysContinue || isContinue(src, s)) {
                    d = new HashMap(s);
                }
                dstMethodAccess.invoke(dst, dstSetIndex, d);
            }
        } else {
            if (isAlwaysContinue || isContinue(src, s)) {
                d.clear();
                d.putAll(s);
            }
        }
    }

    private static boolean isPrimitive(Object clz) {
        try {
            return ((Class<?>) clz.getClass().getField("TYPE").get(null)).isPrimitive();
        } catch (Exception e) {
            return false;
        }
    }

    private static boolean isContinue(Object src, Object srcSub) {
        boolean continueTag = true;
        if (src instanceof Dco && srcSub instanceof Dco) {
            continueTag = ((Dco) src).addCallLayer((Dco) srcSub);
        }
        return continueTag;
    }

    //缓存读写方法、字段名称与方法索引下标
    private static MethodAccess cache(Class<?> clazz, String... ignoreProperties) {
        List<String> ignoreList = (ignoreProperties != null ? Arrays.asList(ignoreProperties) : null);
        synchronized (clazz) {
            MethodAccess methodAccess = MethodAccess.get(clazz);
            //获取该bean所有属性
            Field[] fields = getFieldsByClass(clazz);
            TreeMap<String, Field> fieldTreeMap = new TreeMap<String, Field>();
            for (Field field : fields) {
                // 只处理私有和保护实例属性，不处理类属性与公共变量
                if ((Modifier.isPrivate(field.getModifiers()) || Modifier.isProtected(field.getModifiers()))
                        && !Modifier.isStatic(field.getModifiers())) {

                    String fieldCapitalName = StringUtils.capitalize(field.getName()); // 获取属性名称

                    if (ignoreList != null && ignoreList.contains(fieldCapitalName)) {
                        continue;
                    }

                    String className = clazz.getName();
                    boolean cachField = false;
                    try {
                        String methodName = "get" + fieldCapitalName;
                        final String methodNameTmp = methodName;
                        if (!Arrays.stream(methodAccess.getMethodNames()).anyMatch(p -> p.equals(methodNameTmp))) {
                            methodName = "is" + fieldCapitalName;
                        }

                        int getIndex = methodAccess.getIndex(methodName); // 获取get方法的下标
                        methodIndexMap.put(className + "." + methodName, getIndex); // 将类名get 或者 is方法名，方法下标注册到map中
                        cachField = true;
                    } catch (Exception e) {

                    }
                    try {
                        int setIndex = methodAccess.getIndex("set" + fieldCapitalName); // 获取set方法的下标
                        methodIndexMap.put(className + "." + "set"
                                + fieldCapitalName, setIndex); // 将类名set方法名，方法下标注册到map中
                        cachField = true;
                    } catch (Exception e) {

                    }

                    if (cachField) {
                        fieldTreeMap.put(fieldCapitalName, field); // 将属性名称放入集合里
                    }
                }
            }
            fieldMap.put(clazz, fieldTreeMap); // 将类名，属性名称注册到map中
            methodMap.put(clazz, methodAccess);
            return methodAccess;
        }
    }

    private static Field[] getFieldsByClass(Class t) {
        List<Field> fieldList = new ArrayList<>();
        if (t == null) return new Field[0];
        Class tempClass = t;
        while (tempClass != null && !tempClass.getName().toLowerCase().equals("java.lang.object")) {//当父类为null的时候说明到达了最上层的父类(Object类).
            fieldList.addAll(Arrays.asList(tempClass.getDeclaredFields()));
            tempClass = tempClass.getSuperclass(); //得到父类,然后赋给自己
        }
        return fieldList.toArray(new Field[fieldList.size()]);
    }

}