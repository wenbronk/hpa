package cn.ktt.hpa.commons.response;

/**
 * @Author wenbronk <meng.wen@kangtaitong.cn>
 * @Date 2019-10-17 09:36
 * description:
 */
public enum CommonsCode implements ResultCode {
    /**
     *
     */
    SUCCESS(true, 10000, "操作成功！"),
    SERVER_ERROR(false, 9999, "服务器错误！"),
    TENANTID_FAIL(false, 9998, "tenant_id没有权限！"),
    UNAUTHENTICATED(false, 9997, "操作需要登陆！"),
    UNAUTHORISE(false, 9996, "操作权限不足！"),
    INVALID_PARAM6(false, 9995, "参数非法！"),
    TRANS_PARAM6(false, 9995, "参数类型转换异常")

    ;

    private boolean success;
    private int code;
    private String message;

    private CommonsCode(boolean success, int code, String message) {
        this.success = success;
        this.code = code;
        this.message = message;
    }

    @Override
    public boolean success() {
        return this.success;
    }

    @Override
    public int code() {
        return this.code;
    }

    @Override
    public String message() {
        return this.message;
    }
}
