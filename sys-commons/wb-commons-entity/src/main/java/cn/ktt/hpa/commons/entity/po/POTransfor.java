package cn.ktt.hpa.commons.entity.po;

import cn.ktt.hpa.commons.deepcopy.Dco;
import cn.ktt.hpa.commons.entity.Transfor;
import cn.ktt.hpa.commons.entity.bo.AbstractBO;
import cn.ktt.hpa.commons.entity.TransforException;
import cn.ktt.hpa.commons.entity.vo.AbstractVO;

import java.util.function.Supplier;

/**
 * @Author wenbronk <meng.wen@kangtaitong.cn>
 * @Date 2019-10-17 14:55
 * description:
 */
public interface POTransfor extends Transfor, Dco {

    default   <T extends AbstractBO> T toBO(Class<T> clazz) throws TransforException {
        return transfor(clazz);
    }

    default <T extends AbstractVO> T toVO(Class<T> clazz) throws TransforException {
        return transfor(clazz);
    }
}
