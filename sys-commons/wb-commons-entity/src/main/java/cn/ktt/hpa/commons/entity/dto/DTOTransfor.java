package cn.ktt.hpa.commons.entity.dto;

import cn.ktt.hpa.commons.deepcopy.Dco;
import cn.ktt.hpa.commons.entity.Transfor;
import cn.ktt.hpa.commons.entity.bo.AbstractBO;
import cn.ktt.hpa.commons.entity.TransforException;

import java.util.function.Supplier;

/**
 * @Author wenbronk <meng.wen@kangtaitong.cn>
 * @Date 2019-10-17 14:44
 * description: DTO -> BO
 */
public interface DTOTransfor extends Transfor, Dco {

    default  <T extends AbstractBO> T toBO(Class<T> clazz) throws TransforException {
        return transfor(clazz);
    }

}
