package cn.ktt.hpa.commons.deepcopy.anotations;

import java.lang.annotation.*;

/**
 * @author YanLei <lei.yan@kangtaitong.cn>
 * @version 1.0.0
 * create_time    2019/5/30 16:22
 * description
 */
@Documented
@Inherited
@Target({ElementType.TYPE,ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface CollectionTypeCopy {

    Class<?> value();
    Class<?> elementType();
}
