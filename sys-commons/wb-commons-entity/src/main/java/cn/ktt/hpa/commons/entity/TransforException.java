package cn.ktt.hpa.commons.entity;


import cn.ktt.hpa.commons.exception.AbstractException;
import cn.ktt.hpa.commons.response.ResultCode;

/**
 * @Author wenbronk <meng.wen@kangtaitong.cn>
 * @Date 2019-10-17 11:19
 * description:
 */
public class TransforException extends AbstractException {

    public TransforException(String message) {
        super(message);
    }

    public TransforException(Throwable cause) {
        super(cause);
    }

    public TransforException(ResultCode resultCode) {
        super(resultCode);
    }

    public TransforException(ResultCode resultCode, Throwable cause) {
        super(resultCode, cause);
    }

    public TransforException(ResultCode resultCode, String message, Throwable cause) {
        super(resultCode, message, cause);
    }
}
