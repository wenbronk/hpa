package cn.ktt.hpa.commons.entity.form;

import cn.ktt.hpa.commons.deepcopy.Dco;
import cn.ktt.hpa.commons.entity.Transfor;
import cn.ktt.hpa.commons.entity.bo.AbstractBO;
import cn.ktt.hpa.commons.entity.TransforException;
import cn.ktt.hpa.commons.entity.po.IdEntity;
import lombok.NoArgsConstructor;

import java.util.function.Supplier;

/**
 * @Author wenbronk <meng.wen@kangtaitong.cn>
 * @Date 2019-10-17 14:11
 * description: FORM -> BO
 */
public interface FormTransfor extends Transfor, Dco {

    default  <T extends AbstractBO> T toBO(Class<T> clazz) {
        return transfor(clazz);
    }

    default <T extends IdEntity> T toPO(Class<T> clazz) {
        return transfor(clazz);
    }
}
