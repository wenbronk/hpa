package cn.ktt.hpa.commons.response;

/**
 * @Author wenbronk <meng.wen@kangtaitong.cn>
 * @Date 2019-10-17 09:30
 * description:
 */
public interface ResultCode {
    boolean success();
    int code();
    String message();
}
