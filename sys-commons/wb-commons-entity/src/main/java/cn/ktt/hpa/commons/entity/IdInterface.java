package cn.ktt.hpa.commons.entity;

import java.io.Serializable;

/**
 * @Author wenbronk <meng.wen@kangtaitong.cn>
 * @Date 2019-10-17 17:33
 * description:
 */
public interface IdInterface extends Serializable {

    Long getId();
    void setId(Long id);
}
