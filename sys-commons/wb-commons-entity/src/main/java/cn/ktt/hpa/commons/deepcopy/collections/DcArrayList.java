package cn.ktt.hpa.commons.deepcopy.collections;


import cn.ktt.hpa.commons.deepcopy.Dco;

import java.util.ArrayList;

/**
 * @author LiJunhua <junhua.li@kangtaitong.cn>
 * @version 1.0.0
 * create_time    2019-05-14 2:45
 * description
 */
public class DcArrayList<E extends Dco> extends ArrayList<E> implements DcList<E>,CollectionCommon {
    private final Class<E> clazz;

    public DcArrayList(Class<E> clazz){
        this.clazz = clazz;
    }

    @Override
    public Class<?> getRealType(){
        return clazz;
    }
}