package cn.ktt.hpa.commons.entity.dto;

import cn.ktt.hpa.commons.entity.IdInterface;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Author wenbronk <meng.wen@kangtaitong.cn>
 * @Date 2019-10-17 14:53
 * description:
 */
@Data
public abstract class AbstractDTO implements IdInterface, DTOTransfor {
    protected Long id;
}
