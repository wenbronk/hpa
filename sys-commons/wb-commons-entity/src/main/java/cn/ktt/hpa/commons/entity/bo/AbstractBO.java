package cn.ktt.hpa.commons.entity.bo;

import cn.ktt.hpa.commons.entity.IdInterface;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Author wenbronk <meng.wen@kangtaitong.cn>
 * @Date 2019-10-17 14:15
 * description:
 */
@Data
public abstract class AbstractBO implements BOTransfor, IdInterface {
    private static final long serialVersionUID = 1L;

    protected Long id;

}
