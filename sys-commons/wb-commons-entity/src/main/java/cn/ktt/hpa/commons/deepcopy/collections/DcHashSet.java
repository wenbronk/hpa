package cn.ktt.hpa.commons.deepcopy.collections;


import cn.ktt.hpa.commons.deepcopy.Dco;

import java.util.HashSet;

/**
 * @author LiJunhua <junhua.li@kangtaitong.cn>
 * @version 1.0.0
 * create_time    2019-05-14 2:52
 * description
 */
public class DcHashSet<E extends Dco> extends HashSet<E> implements DcSet<E> {
    private final Class<E> clazz;
    public DcHashSet(Class<E> clazz){
        this.clazz = clazz;
    }
    @Override
    public Class<?> getRealType(){
        return clazz;
    }
}