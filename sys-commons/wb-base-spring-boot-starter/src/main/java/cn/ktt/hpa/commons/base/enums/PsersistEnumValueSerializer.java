package cn.ktt.hpa.commons.base.enums;

import com.alibaba.fastjson.serializer.JSONSerializer;
import com.alibaba.fastjson.serializer.ObjectSerializer;
import com.alibaba.fastjson.serializer.SerializeWriter;

import java.io.IOException;
import java.lang.reflect.Type;

/**
 * @Author lei.yan
 * @Date 2019/11/6 10:32
 * @Version 1.0
 * description:
 */
public class PsersistEnumValueSerializer implements ObjectSerializer {

    @Override
    public void write(JSONSerializer serializer, Object object, Object fieldName, Type fieldType, int features) throws IOException {
        SerializeWriter out = serializer.out;
        if (object instanceof PersistEnum) {
            PersistEnum baseEnum = (PersistEnum) object;

            out.writeFieldValue('{',"name",baseEnum.toString());
            out.writeFieldValue(',',"code",((Integer) baseEnum.getCode()).intValue());
            out.write('}');
        } else {
            out.writeEnum((Enum<?>) object);
        }
    }
}
