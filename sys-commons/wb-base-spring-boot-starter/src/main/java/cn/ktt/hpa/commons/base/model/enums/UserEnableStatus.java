package cn.ktt.hpa.commons.base.model.enums;

import cn.ktt.hpa.commons.base.enums.AbstractEnumConverter;
import cn.ktt.hpa.commons.base.enums.PersistEnum;

/**
 * @Author wenbronk <meng.wen@kangtaitong.cn>
 * @Date 2019-10-15 17:29
 * description:
 */
public enum UserEnableStatus implements PersistEnum<Integer> {

    /**
     *
     */
    WORK("work", 2, "在职"),
    LEAVE("leave", 5, "离职");

    private String name;
    private Integer code;
    private String description;
    private UserEnableStatus(String name, Integer code, String description) {
        this.name = name;
        this.code = code;
        this.description = description;
    }

    @Override
    public Integer getCode() {
        return null;
    }

    public static class Covert extends AbstractEnumConverter<UserEnableStatus, Integer> {}

}