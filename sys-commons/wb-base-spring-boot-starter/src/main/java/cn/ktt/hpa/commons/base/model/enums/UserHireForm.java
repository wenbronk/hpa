package cn.ktt.hpa.commons.base.model.enums;

import cn.ktt.hpa.commons.base.enums.AbstractEnumConverter;
import cn.ktt.hpa.commons.base.enums.PersistEnum;

/**
 * @Author wenbronk <meng.wen@kangtaitong.cn>
 * @Date 2019-10-15 17:28
 * description:
 */
public enum UserHireForm implements PersistEnum<Integer> {
    /**
     *
     */
    CONTRACT("contract", 1, "合同制"),
    OUTSOURCING("outsourcing", 2, "外包"),
    TEMPORARY("temporary", 3, "临时工");

    private String name;
    private Integer code;
    private String description;
    private UserHireForm(String name, Integer code, String description) {
        this.name = name;
        this.code = code;
        this.description = description;
    }

    @Override
    public Integer getCode() {
        return null;
    }

    public static class Covert extends AbstractEnumConverter<UserEnableStatus, Integer> {}
}
