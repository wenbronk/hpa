package cn.ktt.hpa.commons.base.model;

import cn.ktt.hpa.commons.base.model.enums.UserCategory;
import cn.ktt.hpa.commons.base.model.enums.UserEnableStatus;
import cn.ktt.hpa.commons.base.model.enums.UserHireForm;
import cn.ktt.hpa.commons.entity.po.BaseEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.collect.Sets;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

/**
 * @Author wenbronk <meng.wen@kangtaitong.cn>
 * @Date 2019-10-11 16:22
 * description:
 */
@Data
//@Entity
//@Table(name = "sys_user")
@EqualsAndHashCode(callSuper = true)
//@ApiModel(value = "公司信息")
public class SysUser extends BaseEntity {

    @Column(name = "username", columnDefinition = "varchar(20) NOT NULL COMMENT '用户名'")
    private String username;

    @Column(name = "nickname", columnDefinition = "varchar(20) NOT NULL COMMENT '真实姓名'")
    private String nickname;

    @Column(name = "password", columnDefinition = "varchar(20) NOT NULL COMMENT '用户密码'")
    private String password;

    @Column(name = "phone_number", columnDefinition = "varchar(20) NOT NULL COMMENT '手机号码'")
    private String phoneNumber;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "hiredate", columnDefinition = "timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '入职时间'")
    private Date hiredate;

    @Column(name = "work_number", columnDefinition = "varchar(20) NOT NULL COMMENT '工号'")
    private String workNumber;

    @Column(name = "user_enable_status", columnDefinition = "int NOT NULL COMMENT '用户可用状态'")
    private UserEnableStatus userEnableStatus;

    @Column(name = "hire_form", columnDefinition = "int NOT NULL COMMENT '聘用形式'")
    private UserHireForm hireForm;

    /**
     * level
     * String
     * saasAdmin：saas管理员具备所有权限
     * coAdmin：企业管理（创建租户企业的时候添加）
     * user：普通用户（需要分配角色）
     */
    @Column(name = "user_category", columnDefinition = "int NOT NULL COMMENT '用户分类'")
    private UserCategory userCategory;

    /**
     * 使用dateimage记录用户头像
     */
    @Column(name = "staff_photo", columnDefinition = "varchar(20) NOT NULL COMMENT '用户头像'")
    private String staffPhoto;

    /**
     * JsonIgnore
     * : 忽略json转化
     */
    @JsonIgnore
    @ManyToMany
    @JoinTable(name = "sys_user_role", joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "role_id", referencedColumnName = "id")}
    )
    private Set<SysRole> roles = Sets.<SysRole>newHashSet();//用户与角色   多对多\

}
