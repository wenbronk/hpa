package cn.ktt.hpa.commons.base.model.enums;

import cn.ktt.hpa.commons.base.enums.AbstractEnumConverter;
import cn.ktt.hpa.commons.base.enums.PersistEnum;


/**
 * @Author wenbronk <meng.wen@kangtaitong.cn>
 * @Date 2019-10-15 17:29
 */


public enum UserCategory implements PersistEnum<Integer> {
    /**
     * level
     * String
     * saasAdmin：saas管理员具备所有权限
     * coAdmin：企业管理（创建租户企业的时候添加）
     * Customer: 客服， 可任意伪装成别的企业
     * user：普通用户（需要分配角色）
     */
    SAASADMIN("saasAdmin", 1, "系统管理员"),
    CUSTOMER("customer", 2, "客服代表"),
    COMPANYADMIN("companyAdmin", 3, "企业管理员"),
    TEMPORARY("temporary", 4, "临时工");

    private String name;
    private Integer code;
    private String description;

    private UserCategory(String name, Integer code, String description) {
        this.name = name;
        this.code = code;
        this.description = description;
    }

    @Override
    public Integer getCode() {
        return null;
    }

    public static class Covert extends AbstractEnumConverter<UserEnableStatus, Integer> {}
}