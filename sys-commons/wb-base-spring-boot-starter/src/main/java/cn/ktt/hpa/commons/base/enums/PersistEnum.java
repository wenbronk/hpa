package cn.ktt.hpa.commons.base.enums;

/**
 * @Author wenbronk <meng.wen@kangtaitong.cn>
 * @Date 2019-10-12 15:10
 * description:
 */
public interface PersistEnum<CODE> {
    CODE getCode();
}
