package cn.ktt.hpa.commons.base.advicer;

import cn.ktt.hpa.commons.response.BaseResponse;
import cn.ktt.hpa.commons.response.CommonsCode;
import com.alibaba.fastjson.JSONObject;
import org.springframework.http.converter.HttpMessageConversionException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @Author wenbronk
 * @Date 2019/11/6 1:24 下午
 * description:
 */
@RestControllerAdvice
public class FormValidAdvicer {

    @ExceptionHandler(value =  {MethodArgumentNotValidException.class})
    public BaseResponse validateBodyException(MethodArgumentNotValidException exception) {
        BindingResult bindingResult = exception.getBindingResult();

        JSONObject jsonObject = new JSONObject();
        if (bindingResult.hasErrors()) {
            for (ObjectError error : bindingResult.getAllErrors()) {
                FieldError fieldError = (FieldError) error;
                jsonObject.put(fieldError.getField(), fieldError.getDefaultMessage());
            }
        }
        return BaseResponse.getResponse(CommonsCode.INVALID_PARAM6, jsonObject);
    }

    /**
     * 参数类型转换错误
     */
    @ExceptionHandler(value = {HttpMessageConversionException.class})
    public BaseResponse parameterTypeException(HttpMessageConversionException exception) {
        System.out.println("类型转换非法" + exception.getLocalizedMessage());
        String format = String.format("message conversion exception: %s", exception.getLocalizedMessage());
        return BaseResponse.getResponse(CommonsCode.TRANS_PARAM6, format);
    }

}
