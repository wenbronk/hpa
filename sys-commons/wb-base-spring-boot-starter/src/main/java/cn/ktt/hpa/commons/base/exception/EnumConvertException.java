package cn.ktt.hpa.commons.base.exception;

/**
 * @Author wenbronk <meng.wen@kangtaitong.cn>
 * @Date 2019-10-12 15:58
 * description:
 */
public class EnumConvertException extends RuntimeException {
    private static final long serialVersionUID = 6610083281801529147L;

    public EnumConvertException(String msg) {
        super(msg);
    }

    public EnumConvertException(String message, Throwable cause) {
        super(message, cause);
    }
}
