package cn.ktt.hpa.commons.base.enums;

import cn.ktt.hpa.commons.base.exception.EnumConvertException;

import javax.persistence.AttributeConverter;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * @Author wenbronk <meng.wen@kangtaitong.cn>
 * @Date 2019-10-12 15:11
 * description:
 */
public abstract class AbstractEnumConverter<ATTR extends Enum<ATTR> & PersistEnum<CODE>, CODE> implements AttributeConverter<ATTR, CODE> {

    @Override
    public CODE convertToDatabaseColumn(ATTR attribute) {
        return attribute != null ? attribute.getCode() : null;
    }

    @Override
    public ATTR convertToEntityAttribute(CODE code) {
        if (code != null) {
            try {
                Type[] actualTypeArguments = ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments();
                String className = actualTypeArguments[0].getTypeName();
                for (Object obj : Class.forName(className).getEnumConstants()) {
                    ATTR attr = (ATTR) obj;
                    if (code.equals(attr.getCode())) {
                        return attr;
                    }
                }
                throw new UnsupportedOperationException(String.format("枚举转化异常, 枚举类[%s], 数据[%s]", className, code));
            } catch (Exception e) {
                throw new EnumConvertException(e.getMessage(), e.getCause());
            }
        }
        return null;
    }
}
