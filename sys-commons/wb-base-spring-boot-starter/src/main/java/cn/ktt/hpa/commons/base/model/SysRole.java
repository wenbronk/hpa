package cn.ktt.hpa.commons.base.model;

import cn.ktt.hpa.commons.entity.po.BaseEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.*;
import java.util.List;

/**
 * @Author wenbronk <meng.wen@kangtaitong.cn>
 * @Date 2019-10-12 17:31
 * description:
 */
//@Data
//@Entity
//@Table(name = "sys_role")
//@EqualsAndHashCode(callSuper = true)
//@ApiModel(value = "用户角色")
public class SysRole extends BaseEntity {
    private static final long serialVersionUID = 4497149010220586111L;

    @Column(name = "role_name", columnDefinition = "varchar(20) NOT NULL COMMENT '角色名'")
    private String roleName;

    @Column(name = "description", columnDefinition = "varchar(100) NOT NULL COMMENT '角色描述'")
    private String description;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "sys_user_role", joinColumns = {@JoinColumn(name = "role_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")}
    )
    private List<SysUser> sysUserList;

}
