
dependencies {
    implementation(project(":sys-commons:wb-commons-entity"))
    implementation(project(":sys-commons:wb-db-spring-boot-starter"))
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.boot:spring-boot-starter-aop")
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")

    implementation("io.springfox", "springfox-swagger-ui")
    implementation("io.springfox", "springfox-swagger2")

    compile("com.alibaba:fastjson")
}
